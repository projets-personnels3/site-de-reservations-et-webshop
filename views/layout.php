<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9" />
    <meta name="viewport" content="width=device-width, maximum-scale=1"/>
    <title>The Padel Place</title>

    <link rel="stylesheet" media="all" href="<?= SCRIPTS . 'css' . DIRECTORY_SEPARATOR . 'bootstrap.min.css'?>">
    <link rel="stylesheet" media="all" href="<?= SCRIPTS . 'css' . DIRECTORY_SEPARATOR . 'dataTables.bootstrap5.min.css'?>">
    <link rel="stylesheet" media="all" href="<?= SCRIPTS . 'css' . DIRECTORY_SEPARATOR . 'style.css'?>">
    <link rel="stylesheet" media="all" href="<?= SCRIPTS . 'css' . DIRECTORY_SEPARATOR . 'reserver.css'?>">
    <script type="text/javascript" src="<?= SCRIPTS . 'js' . DIRECTORY_SEPARATOR . 'jquery-3.6.0.min.js'?>"></script>
    <script type="text/javascript" src="<?= SCRIPTS . 'js' . DIRECTORY_SEPARATOR . 'ajax.js'?>"></script>
    <script type="text/javascript" src="<?= SCRIPTS . 'js' . DIRECTORY_SEPARATOR . 'bootstrap.bundle.min.js'?>"></script>
    <script type="text/javascript" src="<?= SCRIPTS . 'js' . DIRECTORY_SEPARATOR . 'jquery.dataTables.min.js'?>"></script>
    <script type="text/javascript" src="<?= SCRIPTS . 'js' . DIRECTORY_SEPARATOR . 'dataTables.bootstrap5.min.js'?>"></script>
  </head>
  <body>
    <a name="haut"></a>
    <?php include("../fonctions/menu.php");?>
    <div class="conteneur rounded">
      <?= $content ?>
      <?php include("../fonctions/hautdepage.php");?>
    </div>
  </body>
  <?php include("../fonctions/footer.php");?>
</html>
