<h2>Changement de mot de passe</h2>
<span class="message-message"></span>
<form action="/changermotdepasse" id="changermotdepasseForm" method="post">
<div class="container">
  <div class="row">
    <div class="col-3"></div>
    <div class="col-6">
      <div class="input-group mb-3">
        <span class="input-group-text">
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-key" viewBox="0 0 16 16">
            <path d="M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8zm4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5z"/>
            <path d="M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
          </svg>
        </span>
          <input type="password" class="form-control form-control-lg" name="mot_de_passe" id="mot_de_passe" placeholder="Entrez votre mot de passe actuel" required>
      </div>
    </div>
    <div class="col-3"></div>
  </div>
  <div class="row mt-3">
    <div class="col-3"></div>
    <div class="col-6">
      <div class="input-group mb-3">
        <span class="input-group-text">
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-key-fill" viewBox="0 0 16 16"><path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/></svg>
        </span>
          <input type="password" class="form-control form-control-lg" name="mot_de_passe2" id="mot_de_passe2" placeholder="Entrez votre nouveau mot de passe" required>
      </div>
    </div>
    <div class="col-3"></div>
  </div>
  <div class="row">
    <div class="col-3"></div>
    <div class="col-6">
      <div class="input-group mb-3">
        <span class="input-group-text">
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-key-fill" viewBox="0 0 16 16"><path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/></svg>
        </span>
          <input type="password" class="form-control form-control-lg" name="mot_de_passe3" id="mot_de_passe3" placeholder="Confirmer votre nouveau mot de passe" required>
      </div>
    </div>
    <div class="col-3"></div>
  </div>
  <div class="row mt-5">
    <center><button type="submit" class="btn btn-warning btn-lg">Changer mot de passe</button></center>
  </div>
</div>
</form>
