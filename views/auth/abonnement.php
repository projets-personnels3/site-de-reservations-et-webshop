  <div class="w-75 mx-auto mt-4 text-center">
    <ul class="list-group">
      <li class="list-group-item list-group-item-<?php print ($_SESSION['abonnement'] == 1) ? "success" : "info"; ?>">
        <div class="ms-2 me-auto">
          <div class="fw-bold fs-2">Avantages de l'abonnement <?php print ($_SESSION['abonnement'] == 1) ? "actifs" : ""; ?></div>
        </div>
      </li>
      <li class="list-group-item list-group-item-<?php print ($_SESSION['abonnement'] == 1) ? "success" : "info"; ?>">
        <div class="ms-2 me-auto">
          <div class="fw-bolder fs-4">Affiliation au club de The Padel Place</div>
          Vous pourrez particper aux tournois de la fédération de Padel « AFPadel » et défendre nos couleurs.
        </div>
      </li>
      <li class="list-group-item list-group-item-<?php print ($_SESSION['abonnement'] == 1) ? "success" : "info"; ?>">
        <div class="ms-2 me-auto">
          <div class="fw-bolder fs-4">Participation aux interclubs</div>
          Vous pourrez participer aux interclubs et aux tournois organisés par d'autres clubs.
        </div>
      </li>
      <li class="list-group-item list-group-item-<?php print ($_SESSION['abonnement'] == 1) ? "success" : "info"; ?>">
        <div class="ms-2 me-auto">
          <div class="fw-bolder fs-4">Assurance médicale</div>
          Vous serez couvert en cas de blessures durant vos tournois de la fédération et durant vos réservations chez nous.
        </div>
      </li>
      <li class="list-group-item list-group-item-<?php print ($_SESSION['abonnement'] == 1) ? "success" : "info"; ?>">
        <div class="ms-2 me-auto">
          <div class="fw-bolder fs-4">Réduction sur les réservations de terrains</div>
          Vous aurez un tarif attractif sur vos réservations durant la période de votre abonnement.
        </div>
      </li>
      <li class="list-group-item list-group-item-<?php print ($_SESSION['abonnement'] == 1) ? "success" : "info"; ?>">
        <div class="ms-2 me-auto">
          <div class="fw-bolder fs-4">Avantage sur les réservations de terrains</div>
          Vous pourrez réserver des terrains 2 jours à l'avance que les non affiliés.
        </div>
      </li>
      <li class="list-group-item list-group-item-<?php print ($_SESSION['abonnement'] == 1) ? "success" : "info"; ?>">
        <div class="ms-2 me-auto">
          <form action="/abonnement" method="post">
            <?php if ($_SESSION['abonnement'] == 0) { ?>
                    <input type="hidden" name="abonnement" value="1">
                    <input type="submit" class="btn btn-lg btn-info fs-4" value="S'abonner - <?= $params['prix_abonnement'][0]->prix ?> €">
        <?php    }
                  elseif ($_SESSION['abonnement'] == 1) { ?>
                    <input type="hidden" name="abonnement" value="0">
                    <button type="submit" class="btn btn-danger btn-lg fs-4" onclick="return confirm('Êtes-vous sûr de vouloir vous désabonner et perdre vos avantages ?')">Se désabonner</button>
        <?php    } ?>
          </form>
        </div>
      </li>
    </ul>
  </div>
