<h2>Connexion</h2>
<span class="message-message"></span>
<form action="/login" id="connexionForm" method="post">
  <div class="container-fluid text-center">
    <div class="row">
      <div class="col-4"></div>
      <div class="col-4">
      <div class="input-group mb-3">
        <span class="input-group-text">
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-envelope-fill" viewBox="0 0 16 16"><path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/></svg>
        </span>
          <input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="Entrez votre email" required>
        </div>
      </div>
      <div class="col-4"></div>
    </div>
    <div class="row">
      <div class="col-4"></div>
      <div class="col-4">
        <div class="input-group mb-3">
          <span class="input-group-text">
            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-key-fill" viewBox="0 0 16 16"><path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/></svg>
          </span>
            <input type="password" class="form-control form-control-lg" name="mot_de_passe" id="mot_de_passe" placeholder="Entrez votre mot de passe" required>
        </div>
      </div>
      <div class="col-4"></div>
    </div>
    <div class="row">
      <div class="col-4"></div>
      <div class="col-4">
        <button type="submit" class="btn btn-light btn-lg">Connexion</button>
        <a href="/motdepasseoublie" class="btn btn-outline-light btn-lg">Mot de passe oublié ?</a>
      </div>
      <div class="col-4"></div>
    </div>
  </div>
</form>
