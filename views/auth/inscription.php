<h2>Créer un compte</h2>
<span class="message-message"></span>
<form action="/inscription" id="inscriptionForm" method="post">
<div class="container text-center">
  <div class="row gy-5">
    <div class="col-12">
      <div class="input-group mb-3">
        <span class="input-group-text">
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-person-fill" viewBox="0 0 16 16"><path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/></svg>
        </span>
          <input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder="Entrez votre nom" required>
          <input type="text"class="form-control form-control-lg" name="prenom" id="prenom" placeholder="Entrez votre prénom" required>
      </div>
    </div>
  </div>
  <div class="row gy-5">
    <div class="col-6">
      <div class="input-group mb-3">
        <span class="input-group-text">
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-envelope-fill" viewBox="0 0 16 16"><path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/></svg>
        </span>
          <input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="Entrez votre email" required>
      </div>
    </div>
    <div class="col-6">
      <div class="input-group mb-3">
        <span class="input-group-text">
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-key-fill" viewBox="0 0 16 16"><path d="M3.5 11.5a3.5 3.5 0 1 1 3.163-5H14L15.5 8 14 9.5l-1-1-1 1-1-1-1 1-1-1-1 1H6.663a3.5 3.5 0 0 1-3.163 2zM2.5 9a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/></svg>
        </span>
          <input type="password" class="form-control form-control-lg" name="mot_de_passe" id="mot_de_passe" placeholder="Entrez votre mot de passe" required>
      </div>
    </div>
  </div>
  <div class="row gy-5">
    <div class="col-12">
      <div class="input-group mb-3">
        <span class="input-group-text">
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-house-fill" viewBox="0 0 16 16">
            <path fill-rule="evenodd" d="m8 3.293 6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
            <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
          </svg>
        </span>
        <input type="text" class="form-control form-control-lg" name="adresse" id="adresse" placeholder="Entrez votre adresse">
        <input type="text" class="form-control form-control-lg" name="ville" id="ville" placeholder="Entrez votre ville">
        <input type="text" class="form-control form-control-lg" name="code_postal" id="code_postal" placeholder="Entrez votre code postal">
      </div>
    </div>
  </div>
    <div class="row mt-5">
      <center><button type="submit" class="btn btn-light btn-lg">Créer un compte</button></center>
    </div>
</div>
</form>
