<h2>Réinitialisez votre mot de passe</h2>
<span class="message-message"></span>
<form action="/motdepasseoublie" id="motdepasseoublieForm" method="post">
  <div class="container">
    <div class="row gy-5">
      <center>
      <div class="col-6">
        <div class="input-group mb-3">
          <span class="input-group-text">
            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="#198754" class="bi bi-envelope-fill" viewBox="0 0 16 16"><path d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555ZM0 4.697v7.104l5.803-3.558L0 4.697ZM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757Zm3.436-.586L16 11.801V4.697l-5.803 3.546Z"/></svg>
          </span>
            <input type="email" class="form-control form-control-lg" name="email" id="email" placeholder="Entrez votre email" required>
        </div>
      </div>
    </center>
    </div>
    <div class="row">
      <center><div class="col-8 alert alert-danger">
        <div class="form-check">
          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" required>
          <label class="form-check-label" for="flexCheckDefault">
            <b>Attention !</b> Un nouveau mot de passe vous sera envoyé sur votre adresse email. Confirmez-vous la réinitialisation de votre mot de passe ?
          </label>
        </div>
      </div></center>
    </div>
    <div class="row">
      <center><button type="submit" class="btn btn-danger btn-lg">Réinitialisez votre mot de passe</button></center>
    </div>
  </div>
</form>
