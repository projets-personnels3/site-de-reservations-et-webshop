<?php
$images_json = json_encode($params['imagesdisponibles']);
$marques_json = json_encode($params['marques']);
$ref_marque_json = json_encode($params['article']->ref_marques);
$images_articles_json = json_encode($params['images_articles']);
?>

<script type="text/javascript">

var id = <?php Print($params['article']->id); ?>;
var compteur_images = 0;

window.addEventListener('load', function () {
  var images_json = <?php print $images_json;?> ;
  var marques_json = <?php print $marques_json;?> ;
  var images_articles_json = <?php print $images_articles_json;?> ;

  images_json.forEach(display_images);
  marques_json.forEach(display_marques);
  images_articles_json.forEach(display_carousel);
  images_articles_json.forEach(display_images_dissocier);
});

function display_images(item) {
 document.getElementById('ajouter-images').innerHTML += '<input type="checkbox" class="btn-check" name="ref_images[]" id="btn-check-'+item.id+'" value="'+item.id+'"><label class="btn btn-success btn-lg w-100 mb-2" for="btn-check-'+item.id+'"><img src="'+item.path+'" alt="" width="200px" height="200px" class="rounded"></label>';
}

function display_images_dissocier(item) {
 document.getElementById('enlever-images').innerHTML += '<input type="checkbox" class="btn-check" name="ref_images[]" id="btn-check-'+item.id+'" value="'+item.id+'"><label class="btn btn-success btn-lg w-100 mb-2" for="btn-check-'+item.id+'"><img src="'+item.path+'" alt="" width="200px" height="200px" class="rounded"></label>';
}

function display_carousel(item) {

  if (compteur_images == 0) {
    document.getElementById('carousel-images').innerHTML += '<div class="carousel-item active"><img src="'+item.path+'" class="card-img-top" alt="Erreur chargement d\'image" height="400px"></div>';
  }else {
    document.getElementById('carousel-images').innerHTML += '<div class="carousel-item"><img src="'+item.path+'" class="card-img-top" alt="Erreur chargement d\'image" height="400px"></div>';
  }
  compteur_images++;
}

function display_marques(item) {
  if (item.id == <?php print $ref_marque_json;?>) {
    document.getElementById('ref_marques').innerHTML += '<option value ="'+item.id+'" selected>'+item.nom+'</option>';
  }else {
    document.getElementById('ref_marques').innerHTML += '<option value ="'+item.id+'">'+item.nom+'</option>';
  }
}

$(document).ready(function(){

  $("#ADMINeditimageassocForm").on("submit",function(e){
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        url : "/admin/editArticle/"+ id,
        type: "POST",
        cache:false,
        dataType: "json",
        data: formData,
        success:function(response){
          data = JSON.parse(JSON.stringify(response));
          if (data.error == "0") {
            $("#ADMINeditimageassocForm").trigger("reset");
            $('.message-image').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
             var y = document.getElementById("ajouter-images");
             y.innerHTML = "";
             data.images.forEach(display_images);
             if (data.images) {
               $('#associer').replaceWith('<span id="associer"><button class="btn btn-info btn-lg h-100 d-flex mx-auto align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">Associer image(s)</button></span>');
             }
             if (data.images.length == 0) {
               $('#associer').replaceWith('<span id="associer"><div class="alert alert-danger text-center">Il n\'y a pas d\'images disponibles. Ajoutez-en afin de pouvoir les associer.</div></span>');
             }
             var z = document.getElementById("enlever-images");
             z.innerHTML = "";
             data.images_articles.forEach(display_images_dissocier);
             if (data.images_articles) {
               $('#dissocier-bouton').replaceWith('<span id="dissocier-bouton"><button class="btn btn-info btn-lg h-100 d-flex mx-auto align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">Dissocier image(s)</button></span>');
               $('#dissocier-carousel').replaceWith('<span id="dissocier-carousel"><div id="carouselExampleIndicators" class="carousel carousel-dark slide mx-auto mb-2" data-bs-ride="carousel" style="width: 400px;"><div class="carousel-inner rounded" id="carousel-images"></div><button class="carousel-control-prev rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="visually-hidden">Previous</span></button><button class="carousel-control-next rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="visually-hidden">Next</span></button></div></span>');
             }
             if (data.images_articles.length == 0) {
               $('#dissocier-bouton').replaceWith('<span id="dissocier-bouton"><div class="alert alert-danger text-center">Il n\'y a pas d\'image associée à l\'article.</div></span>');
               $('#dissocier-carousel').replaceWith('<span id="dissocier-carousel"><div class="alert alert-danger text-center">Il n\'y a pas d\'image associée à l\'article.</div></span>');
             }
             var x = document.getElementById("carousel-images");
             x.innerHTML = "";
             compteur_images = 0;
             data.images_articles.forEach(display_carousel);
          }
          else if(data.error == "1") {
           $('.message-image').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
          }
        }
    });
  });

  $("#ADMINeditarticleimageForm").on("submit",function(e){
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        url : "/admin/editArticle/"+ id,
        type: "POST",
        dataType: "json",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success:function(response){
          data = JSON.parse(JSON.stringify(response));
          if (data.error == "0") {
            $("#ADMINeditarticleimageForm").trigger("reset");
            $('.message-image').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
             var x = document.getElementById("ajouter-images");
             x.innerHTML = "";
             data.images.forEach(display_images);
             if (data.images) {
               $('#associer').replaceWith('<span id="associer"><button class="btn btn-info btn-lg h-100 d-flex mx-auto align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">Associer image(s)</button></span>');
             }
          }
          else if(data.error == "1") {
           $('.message-image').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
          }
        }
    });
  });

  $("#ADMINeditimagedissocForm").on("submit",function(e){
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        url : "/admin/editArticle/"+ id,
        type: "POST",
        cache:false,
        dataType: "json",
        data: formData,
        success:function(response){
          data = JSON.parse(JSON.stringify(response));
          if (data.error == "0") {
            $("#ADMINeditimagedissocForm").trigger("reset");
            $('.message-image').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
             var y = document.getElementById("ajouter-images");
             y.innerHTML = "";
             data.images.forEach(display_images);
             if (data.images) {
               $('#associer').replaceWith('<span id="associer"><button class="btn btn-info btn-lg h-100 d-flex mx-auto align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">Associer image(s)</button></span>');
             }
             if (data.images.length == 0) {
               $('#associer').replaceWith('<span id="associer"><div class="alert alert-danger text-center">Il n\'y a pas d\'images disponibles. Ajoutez-en afin de pouvoir les associer.</div></span>');
             }
             var z = document.getElementById("enlever-images");
             z.innerHTML = "";
             data.images_articles.forEach(display_images_dissocier);
             if (data.images_articles) {
               $('#dissocier-bouton').replaceWith('<span id="dissocier-bouton"><button class="btn btn-info btn-lg h-100 d-flex mx-auto align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">Dissocier image(s)</button></span>');
               $('#dissocier-carousel').replaceWith('<span id="dissocier-carousel"><div id="carouselExampleIndicators" class="carousel carousel-dark slide mx-auto mb-2" data-bs-ride="carousel" style="width: 400px;"><div class="carousel-inner rounded" id="carousel-images"></div><button class="carousel-control-prev rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev"><span class="carousel-control-prev-icon" aria-hidden="true"></span><span class="visually-hidden">Previous</span></button><button class="carousel-control-next rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next"><span class="carousel-control-next-icon" aria-hidden="true"></span><span class="visually-hidden">Next</span></button></div></span>');
             }
             if (data.images_articles.length == 0) {
               $('#dissocier-bouton').replaceWith('<span id="dissocier-bouton"><div class="alert alert-danger text-center">Il n\'y a pas d\'image associée à l\'article.</div></span>');
               $('#dissocier-carousel').replaceWith('<span id="dissocier-carousel"><div class="alert alert-danger text-center">Il n\'y a pas d\'image associée à l\'article.</div></span>');
             }
             var x = document.getElementById("carousel-images");
             x.innerHTML = "";
             compteur_images = 0;
             data.images_articles.forEach(display_carousel);
          }
          else if(data.error == "1") {
           $('.message-image').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
          }
        }
    });
  });

  $("#ADMINeditarticlemarqueForm").on("submit",function(e){
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        url : "/admin/editArticle/"+ id,
        type: "POST",
        cache:false,
        dataType: "json",
        data: formData,
        success:function(response){
          data = JSON.parse(JSON.stringify(response));
          if (data.error == "0") {
            $("#ADMINeditarticlemarqueForm").trigger("reset");
            $('.message-marque').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-marque"></span>');
             var x = document.getElementById("ref_marques");
             x.innerHTML = "";
             x.innerHTML = '<option value="" selected disabled>Choississez une marque</option>';
             data.marques.forEach(display_marques);
          }
          else if(data.error == "1") {
           $('.message-marque').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-marque"></span>');
          }
        }
    });
  });

  $("#ADMINeditarticleForm").on("submit",function(e){
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        url : "/admin/editArticle/" + id,
        type: "POST",
        cache:false,
        dataType: "json",
        data: formData,
        success:function(response){
          data = JSON.parse(JSON.stringify(response));
          if (data.error == "0") {
            $("#ADMINeditarticleForm").trigger("reset");
            $('.message-article').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-article"></span>');
             var x = document.getElementById("ajouter-images");
             x.innerHTML = "";
             data.images.forEach(display_images);
             if (data.images.length == 0) {
               $('#associer').replaceWith('<span id="associer"><div class="alert alert-danger text-center">Il n\'y a pas d\'images disponibles. Ajoutez-en afin de pouvoir les associer.</div></span>');
             }
          }
          else if(data.error == "1") {
           $('.message-article').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-article"></span>');
          }
        }
    });
  });

});

</script>

<h2>Modifier article #<?= $params['article']->id ?></h2>
<div class="container-fluid text-black">
  <div class="row border rounded">
    <div class="col-6 text-center">
      <h3 class="text-white text-center">Associer/Ajouter/Dissocier une ou plusieurs image(s)*</h3>
      <hr class="text-white">
      <span class="message-image"></span>
      <div class="row">
        <div class="col-3">
          <span id="associer">
            <?php if (empty($params['imagesdisponibles'])) {?>
              <div class="alert alert-danger text-center">
                Il n'y a pas d'images disponibles. Ajoutez-en afin de pouvoir les associer.
              </div>
            <?php } else { ?>
            <button class="btn btn-info btn-lg h-100 d-flex mx-auto align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
              Associer image(s)
            </button>
          <?php } ?>
        </span>
          <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
            <div class="offcanvas-header">
              <h3 class="offcanvas-title" id="offcanvasExampleLabel">Choississez une ou plusieurs image(s) à ajouter à l'article</h3>
              <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
              <form action="/admin/editArticle/<?= $params['article']->id ?>" id="ADMINeditimageassocForm" method="post">
                <input type="submit" class="btn btn-info btn-lg w-100 mb-3" value="Confirmer choix des images" data-bs-dismiss="offcanvas">
                <input type="hidden" name="associer" value="">
                <div class="checkbox-group required">
                    <div class="form-check ps-0" id="ajouter-images"></div>
                </div>
                <input type="submit" class="btn btn-info btn-lg w-100 mt-3" value="Confirmer choix des images" data-bs-dismiss="offcanvas">
              </form>
            </div>
          </div>
        </div>
        <div class="col-6 text-center border-start">
          <form action="/admin/editArticle/<?= $params['article']->id ?>" id="ADMINeditarticleimageForm" method="post" enctype="multipart/form-data">
              <input type="file" class="form-control form-control-lg mx-auto mb-3" name="path" id="path" required>
              <input type="submit" class="btn btn-info btn-lg w-100" value="Ajouter une nouvelle image">
          </form>
        </div>
        <div class="col-3 border-start">
          <span id="dissocier-bouton">
            <?php if (empty($params['images_articles'])) {?>
              <div class="alert alert-danger text-center">
                Il n'y a pas d'images à dissocier.
              </div>
            <?php } else { ?>
            <button class="btn btn-info btn-lg h-100 d-flex mx-auto align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">
              Dissocier image(s)
            </button>
            <?php } ?>
          </span>
          <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
            <div class="offcanvas-header">
              <h3 class="offcanvas-title" id="offcanvasRightLabel">Choississez une ou plusieurs image(s) à enlever à l'article</h3>
              <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
              <form action="/admin/editArticle/<?= $params['article']->id ?>" id="ADMINeditimagedissocForm" method="post">
                <input type="submit" class="btn btn-info btn-lg w-100 mb-3" value="Confirmer choix des images" data-bs-dismiss="offcanvas">
                <input type="hidden" name="dissocier" value="">
                <div class="checkbox-group required">
                    <div class="form-check ps-0" id="enlever-images"></div>
                </div>
                <input type="submit" class="btn btn-info btn-lg w-100 mt-3" value="Confirmer choix des images" data-bs-dismiss="offcanvas">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-6 border-start">
      <h3 class="text-white text-center">Ajouter une marque*</h3>
      <hr class="text-white">
      <span class="message-marque"></span>
      <form action="/admin/editArticle/<?= $params['article']->id ?>" id="ADMINeditarticlemarqueForm" method="post">
        <div class="text-center">
          <div class="form-floating w-50 mx-auto">
            <input type="text" class="form-control form-control-lg mb-3" name="nom" id="nom" placeholder=" " required>
            <label for="nom">Encodez le nom de la nouvelle marque</label>
            <input type="submit" class="btn btn-info btn-lg w-100" value="Ajouter une nouvelle marque">
          </div>
        </div>
      </form>
    </div>
    <p class="text-center text-white">*Optionnel</p>
  </div>
  <hr>
  <div class="row border rounded">
      <h3 class="text-white text-center">Prévisualisation du carroussel d'image(s) associée(s) à l'article #<?= $params['article']->id ?></h3>
      <hr class="text-white">
      <span id="dissocier-carousel">
      <?php if (empty($params['images_articles'])) {?>
        <div class="alert alert-danger text-center">
          Il n'y a pas d'image associée à l'article.
        </div>
      <?php } else { ?>
        <div id="carouselExampleIndicators" class="carousel carousel-dark slide mx-auto mb-2" data-bs-ride="carousel" style="width: 400px;">
          <div class="carousel-inner rounded" id="carousel-images"></div>
            <button class="carousel-control-prev rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
      <?php } ?>
  </span>
  </div>
  <hr>
  <div class="row border rounded">
    <h3 class="text-white text-center mt-3">Modifier l'article #<?= $params['article']->id ?></h3>
    <hr class="text-white">
    <span class="message-article"></span>
    <form action="/admin/editArticle/<?= $params['article']->id ?>" id="ADMINeditarticleForm" method="post">
      <div class="row">
        <div class="col">
          <div class="form-floating mb-3">
            <input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder=" " value="<?= $params['article']->nom?>" required>
            <label for="nom">Nom de l'article #<?= $params['article']->id ?></label>
          </div>
        </div>
        <div class="col">
          <div class="form-floating">
            <textarea class="form-control form-control-lg" name="description" id="description" placeholder=" " required><?= $params['article']->description?></textarea>
            <label for="description">Description de l'article #<?= $params['article']->id ?></label>
          </div>
        </div>
      </div>
      <div class="row mt-3">
        <div class="col">
          <div class="form-floating">
            <select class="form-select form-select-lg" name="ref_marques" id="ref_marques" required>
              <option value="" disabled>Choississez une marque</option>
            </select>
            <label for="ref_marques">Marque de l'article #<?= $params['article']->id ?></label>
          </div>
        </div>
        <div class="col">
          <div class="form-floating mb-3">
            <input type="number" class="form-control form-control-lg" name="prix" id="prix" placeholder=" " value="<?= $params['article']->prix?>" min="1" max="1000" required>
            <label for="prix">Prix de l'article #<?= $params['article']->id ?></label>
          </div>
        </div>
        <div class="col">
          <div class="form-floating">
            <select class="form-select form-select-lg" name="ref_categories" id="ref_categories" required>
              <option value="" disabled>Choississez une catégorie</option>
              <?php foreach ($params['categories'] as $categorie) { ?>
              <option <?php echo($params['article']->ref_categories == $categorie->id) ? "selected" : ""; ?> value ="<?= $categorie->id ?>"> <?= $categorie->nom ?></option>
              <?php  } ?>
            </select>
            <label for="ref_categories">Catégorie de l'article #<?= $params['article']->id ?></label>
          </div>
        </div>
      </div>
      <div class="row text-center">
        <div class="col">
          <input type="submit" class="btn btn-warning btn-lg mb-2 mt-4 w-25" value="Modifier l'article #<?= $params['article']->id ?>">
        </div>
      </div>
    </form>
  </div>
</div>
