<script type="text/javascript">
    var id = <?php Print($params['reservation']->id); ?>;
</script>

<h2>Modifier la réservation #<?= $params['reservation']->id ?> de <?= $params['utilisateur']->nom ?> <?= $params['utilisateur']->prenom ?></h2>
<span class="message-message"></span>
<div class="container">
  <form action="/admin/editReservation/<?= $params['reservation']->id ?>" id="ADMINeditreservationForm" method="post">
    <input type="hidden" name="ref_utilisateurs" value="<?= $params['reservation']->ref_utilisateurs ?>">
  <div class="row cadrevert rounded">
    <div class="col-1">
      <h2>1.</h2>
    </div>
    <div class="col-8">
      <input type="date" class="form-control form-control-lg" name="dates" value="<?= $params['reservation']->dates ?>" required>
    </div>
    <div class="col-3">
      <input type='button' class="btn btn-lg btn-info col-12" value='Confirmer choix date' onclick='go()' />
    </div>
  </div>
  <div class="row cadrevert rounded">
    <div class="col-1">
      <h2>2.</h2>
    </div>
    <div class="col-11">
      <select class="form-select form-select-lg"  name="ref_horaires">
        <option disabled>--Choississez un créneau horaire--</option>
        <?php foreach ($params['horaires'] as $horaires):
                if ($params['horaire']->creneaux == $horaires->creneaux)
                { ?>
                  <option value="<?= $horaires->id ?>" selected><?= $horaires->creneaux ?></option>
          <?php }
                else
                { ?>
                  <option value="<?= $horaires->id ?>"><?= $horaires->creneaux ?></option>
          <?php }
              endforeach ?>
      </select>
    </div>
  </div>
  <div class="row cadrevert rounded">
    <div class="col-1">
      <h2>3.</h2>
    </div>
    <div class="col-11">
      <select class="form-select form-select-lg" name="ref_terrains">
        <option disabled>--Choississez un terrain--</option>
  <?php foreach ($params['terrains'] as $terrains):
          if ($params['terrain']->nom == $terrains->nom)
          { ?>
            <option value="<?= $terrains->id ?>" selected><?= $terrains->nom ?></option>
    <?php }
          else
          { ?>
            <option value="<?= $terrains->id ?>"><?= $terrains->nom ?></option>
    <?php }
        endforeach ?>
      </select>
    </div>
  </div>
    <div class="row mt-4">
      <center><button class="btn btn-warning btn-lg" type="submit">Modifier la réservation</button></center>
    </div>
  </form>
</div>
