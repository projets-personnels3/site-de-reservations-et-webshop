<script type="text/javascript">
$(document).ready(function() {
    $('table.table').DataTable( {
        "language": {
            "lengthMenu": "Afficher _MENU_ lignes par page",
            "zeroRecords": "Il n'y a aucune ligne dans ce tableau",
            "info": "Affiche la page _PAGE_ sur _PAGES_",
            "infoEmpty": "Tableau vide",
            "infoFiltered": "(filtré parmi _MAX_ entrées)",
            "search": 'Chercher :',
            "paginate":{"previous":'précédente',"next":'suivante'}
        }
    } );
} );
</script>


<h2>Administration</h2>
 <?php if (!isset($_GET['success']))
 { ?>
   <center><div class="alert alert-danger" role="alert">
   Il faut être connecté avec un compte administrateur pour accéder à cette page !
   </div></center>
 <?php
}elseif (isset($_GET['success'])) {
 ?>
 <div class="accordion" id="accordionExample">
   <div class="accordion-item" style="background-color: #198754;">
     <h2 class="accordion-header" id="headingEight">
       <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
         Gestion du prix d'un abonnement
       </button>
     </h2>
     <div id="collapseEight" class="accordion-collapse collapse show" aria-labelledby="headingEight">
       <div class="accordion-body">
         <span class="message-message"></span>
         <form class="row" action="/admin?success=true" id="ADMINabonnementForm" method="post">
           <div class="col-auto">
             <label for="prix" class="col-form-label">Prix d'un abonnement en Euros : </label>
           </div>
           <div class="col-auto">
             <input class="form-control form-control-lg mx-auto" type="number" name="prix" id="prix" value="<?= $params['abonnement'][0]->prix ?>" placeholder="Définissez un prix pour l'abonnement" required>
           </div>
           <div class="col-auto">
             <input class="btn btn-warning btn-lg mx-auto" type="submit" value="Modifier prix abonnement">
           </div>
         </form>
       </div>
     </div>
   </div>
   <div class="accordion-item" style="background-color: #198754;">
     <h2 class="accordion-header" id="headingNine">
       <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseNine" aria-expanded="true" aria-controls="collapseNine">
         Gestion des terrains
       </button>
     </h2>
     <div id="collapseNine" class="accordion-collapse collapse show" aria-labelledby="headingNine">
       <div class="accordion-body">
         <div class="text-center"><a href="/admin/newTerrain" class="btn btn-light">AJOUTER UN TERRAIN</a></div>
         <table class="table table-hover table-bordered" id="">
           <thead>
             <tr class="table-light">
               <th scope="col">#</th>
               <th scope="col">Nom</th>
               <th scope="col">Types Terrains</th>
               <th scope="col">Actions</th>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($params['terrains'] as $terrains){
           ?><tr>
           <td><?= $terrains->Id ?></td>
           <td><?= $terrains->Nom ?></td>
           <td><?= $terrains->Types_terrains ?></td>
           <td><a href="/admin/editTerrain/<?= $terrains->Id ?>" class="btn btn-warning" >Modifier</a>
               <form action="/admin/deleteTerrain/<?= $terrains->Id ?>" method="post" class="d-inline">
               <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce terrain ?')">Supprimer</button></form>
           </td>
           </tr>
           <?php } ?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
   <div class="accordion-item" style="background-color: #198754;">
     <h2 class="accordion-header" id="headingSeven">
       <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
         Gestion des types de réservations
       </button>
     </h2>
     <div id="collapseSeven" class="accordion-collapse collapse show" aria-labelledby="headingSeven">
       <div class="accordion-body">
         <div class="text-center"><a href="/admin/newTypeReservation" class="btn btn-light">AJOUTER UN TYPE DE RÉSERVATION</a></div>
         <table class="table table-hover table-bordered" id="">
           <thead>
             <tr class="table-light">
               <th scope="col">#</th>
               <th scope="col">Nom</th>
               <th scope="col">Nombre de personne(s)</th>
               <th scope="col">Prix Normal</th>
               <th scope="col">Prix Abonnés</th>
               <th scope="col">Actions</th>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($params['types_reservations'] as $types_reservations){
           ?><tr>
           <td><?= $types_reservations->id ?></td>
           <td><?= $types_reservations->nom ?></td>
           <td><?= $types_reservations->nombre_personnes ?></td>
           <td><?= $types_reservations->prix ?> €</td>
           <td><?= $types_reservations->prix_abonne ?> €</td>
           <td><a href="/admin/editTypeReservation/<?= $types_reservations->id ?>" class="btn btn-warning" >Modifier</a>
               <form action="/admin/deleteTypeReservation/<?= $types_reservations->id ?>" method="post" class="d-inline">
               <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce type de réservations ?')">Supprimer</button></form>
           </td>
           </tr>
           <?php } ?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
   <div class="accordion-item" style="background-color: #198754;">
     <h2 class="accordion-header" id="headingOne">
       <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
         Gestion des réservations
       </button>
     </h2>
     <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne">
       <div class="accordion-body">
         <table class="table table-hover table-bordered" id="">
           <thead>
             <tr class="table-light">
               <th scope="col">#</th>
               <th scope="col">Nom</th>
               <th scope="col">Prénom</th>
               <th scope="col">Email</th>
               <th scope="col">Date</th>
               <th scope="col">Terrain</th>
               <th scope="col">Créneau</th>
               <th scope="col">Type Réservations</th>
               <th scope="col">Coût</th>
               <th scope="col">Actions</th>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($params['reservations'] as $reservation){
           ?><tr>
           <td><?= $reservation->Id ?></td>
           <td><?= $reservation->Nom ?></td>
           <td><?= $reservation->Prenom ?></td>
           <td><?= $reservation->Email ?></td>
           <td><?php $date=date_create($reservation->Dates);echo date_format($date,"d/m/Y"); ?></td>
           <td><?= $reservation->Terrain ?></td>
           <td><?= $reservation->Creneau ?></td>
           <td><?= $reservation->Types ?></td>
           <td><?= $reservation->Cout ?> €</td>
           <td><a href="/admin/editReservation/<?= $reservation->Id ?>" class="btn btn-warning" >Modifier</a>
               <form action="/admin/deleteReservation/<?= $reservation->Id ?>" method="post" class="d-inline">
                 <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cette réservation ?')">Supprimer</button></form>
           </td>
           </tr>
         <?php }?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
   <div class="accordion-item"  style="background-color: #198754;">
     <h2 class="accordion-header" id="headingTwo">
       <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
         Gestion des utilisateurs
       </button>
     </h2>
     <div id="collapseTwo" class="accordion-collapse collapse show" aria-labelledby="headingTwo">
       <div class="accordion-body">
         <table class="table table-hover table-bordered" id="">
           <thead>
             <tr class="table-light">
               <th scope="col">#</th>
               <th scope="col">Nom</th>
               <th scope="col">Prénom</th>
               <th scope="col">Email</th>
               <th scope="col">Adresse</th>
               <th scope="col">Ville</th>
               <th scope="col">Code postal</th>
               <th scope="col">Admin ?</th>
               <th scope="col">Abonné ?</th>
               <th scope="col">Actions</th>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($params['utilisateurs'] as $utilisateur){?>
           <tr>
           <td><?= $utilisateur->id ?></td>
           <td><?= $utilisateur->nom ?></td>
           <td><?= $utilisateur->prenom ?></td>
           <td><?= $utilisateur->email ?></td>
           <td><?= $utilisateur->adresse ?></td>
           <td><?= $utilisateur->ville ?></td>
           <td><?= $utilisateur->code_postal ?></td>
           <td><?php if($utilisateur->admin == 1){?>Oui<?php }else {?>/<?php }?></td>
           <td><?php if($utilisateur->abonnement == 1){?>Oui<?php }else {?>/<?php }?></td>
           <td><a href="/admin/editUtilisateur/<?= $utilisateur->id ?>" class="btn btn-warning" >Modifier rôle/abonnement</a></td>
           </tr>
           <?php } ?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
   <div class="accordion-item" style="background-color: #198754;">
     <h2 class="accordion-header" id="headingThree">
       <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
         Gestion des articles</button>
     </h2>
     <div id="collapseThree" class="accordion-collapse collapse show" aria-labelledby="headingThree">
       <div class="accordion-body">
         <div class="text-center"><a href="/admin/newArticle" class="btn btn-light">AJOUTER UN ARTICLE</a></div>
         <table class="table table-hover table-bordered" id="">
           <thead>
             <tr class="table-light">
               <th scope="col">#</th>
               <th scope="col">Nom</th>
               <th scope="col">Images</th>
               <th scope="col">Description</th>
               <th scope="col">Prix</th>
               <th scope="col">Catégorie</th>
               <th scope="col">Marque</th>
               <th scope="col">Actions</th>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($params['articles'] as $articles){
           ?><tr>
           <td><?= $articles->Id ?></td>
           <td><?= $articles->Nom ?></td>
           <td><?php include("../fonctions/carousel_articles.php");?></td>
           <td><?= $articles->Description ?></td>
           <td><?= $articles->Prix ?>€</td>
           <td><?= $articles->Categorie ?></td>
           <td><?= $articles->Marque ?></td>
           <td><a href="/admin/editArticle/<?= $articles->Id ?>" class="btn btn-warning" >Modifier</a>
               <form action="/admin/deleteArticle/<?= $articles->Id ?>" method="post" class="d-inline">
               <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cet article ?')">Supprimer</button></form>
           </td>
           </tr>
           <?php } ?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
   <div class="accordion-item" style="background-color: #198754;">
     <h2 class="accordion-header" id="headingFour">
       <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
         Gestion des images
       </button>
     </h2>
     <div id="collapseFour" class="accordion-collapse collapse show" aria-labelledby="headingFour">
       <div class="accordion-body">
         <div class="text-center"><a href="/admin/newImage" class="btn btn-light">AJOUTER UNE IMAGE</a></div>
         <table class="table table-hover table-bordered" id="">
           <thead>
             <tr class="table-light">
               <th scope="col">#</th>
               <th scope="col">Images</th>
               <th scope="col">Actions</th>
               <th scope="col">Actions images/articles</th>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($params['images'] as $image){
           ?><tr>
           <td><?= $image->id ?></td>
           <td><img src="<?= $image->path ?>" alt="image" width="100" height="100"></td>
           <td><a href="/admin/editImage/<?= $image->id ?>" class="btn btn-warning">Modifier</a>
               <form action="/admin/deleteImage/<?= $image->id ?>" method="post" class="d-inline">
                <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cette image ?')">Supprimer</button></form>
              </td>
                <td><?php foreach ($params['images_articles'] as $images_articles){

                     if ($image->id == $images_articles->ref_images && $image->associee == 1){ ?>
                       <form action="/admin/deleteImageArticle/<?= $images_articles->id ?>" method="post" class="d-inline">
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer l\'association de cette image à un article ?')">Supprimer association image/article</button></form>

                        <?php break; }
                        elseif ($image->id != $images_articles->ref_images && $image->associee == 0){ ?>
                               <a href="/admin/imageArticle/<?= $image->id ?>" class="btn btn-info">Association images/articles</a>
                     <?php
                    break;  }
                    }?>
           </td>
           </tr>
           <?php } ?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
   <div class="accordion-item" style="background-color: #198754;">
     <h2 class="accordion-header" id="headingFive">
       <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
         Gestion des marques
       </button>
     </h2>
     <div id="collapseFive" class="accordion-collapse collapse show" aria-labelledby="headingFive">
       <div class="accordion-body">
         <div class="text-center"><a href="/admin/newMarque" class="btn btn-light">AJOUTER UNE MARQUE</a></div>
         <table class="table table-hover table-bordered" id="">
           <thead>
             <tr class="table-light">
               <th scope="col">#</th>
               <th scope="col">Nom</th>
               <th scope="col">Actions</th>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($params['marques'] as $marque){
           ?><tr>
           <td><?= $marque->id ?></td>
           <td><?= $marque->nom ?></td>
           <td><a href="/admin/editMarque/<?= $marque->id ?>" class="btn btn-warning" >Modifier</a>
               <form action="/admin/deleteMarque/<?= $marque->id ?>" method="post" class="d-inline">
               <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cette marque ?')">Supprimer</button></form>
           </td>
           </tr>
           <?php } ?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
   <div class="accordion-item" style="background-color: #198754;">
     <h2 class="accordion-header" id="headingSix">
       <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="true" aria-controls="collapseSix">
         Gestion des commandes
       </button>
     </h2>
     <div id="collapseSix" class="accordion-collapse collapse show" aria-labelledby="headingSix">
       <div class="accordion-body">
         <table class="table table-hover table-bordered" id="">
           <thead>
             <tr class="table-light">
               <th scope="col">#</th>
               <th scope="col">Dates</th>
               <th scope="col">Clients</th>
               <th scope="col">Statuts</th>
               <th scope="col">Actions</th>
             </tr>
           </thead>
           <tbody>
             <?php foreach ($params['commandes'] as $commande){ ?>
              <tr>
               <td><?= $commande->Id ?></td>
               <td><?= $commande->Dates ?></td>
               <td><?= $commande->Clients ?></td>
               <td><?= $commande->Statuts ?></td>
               <td><a href="/admin/detailsCommande/<?= $commande->Id ?>" class="btn btn-info">Détails</a>
                   <form action="/admin/deleteCommande/<?= $commande->Id ?>" method="post" class="d-inline">
                   <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir supprimer cette commande ?')">Supprimer</button></form>
               </td>
             </tr>
           <?php } ?>
           </tbody>
         </table>
       </div>
     </div>
   </div>
 </div>
<?php } ?>
