<?php
$images_json = json_encode($params['imagesdisponibles']);
$marques_json = json_encode($params['marques']);
?>

<script type="text/javascript">

window.addEventListener('load', function () {
  var images_json = <?php print $images_json;?> ;
  var marques_json = <?php print $marques_json;?> ;

  images_json.forEach(display_images);
  marques_json.forEach(display_marques);
});

function display_images(item) {
 document.getElementById('afficher-images').innerHTML += '<input type="checkbox" class="btn-check" name="ref_images[]" id="btn-check-'+item.id+'" value="'+item.id+'"><label class="btn btn-success btn-lg w-100 mb-2" for="btn-check-'+item.id+'"><img src="'+item.path+'" alt="" width="200px" height="200px" class="rounded"></label>';
}

function display_marques(item) {
 document.getElementById('ref_marques').innerHTML += '<option value ="'+item.id+'">'+item.nom+'</option>';
}

$(document).ready(function(){

  $("#ADMINnewimageassocForm").on("submit",function(e){
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        url : "/admin/newArticle",
        type: "POST",
        cache:false,
        dataType: "json",
        data: formData,
        success:function(response){
          data = JSON.parse(JSON.stringify(response));
          if (data.error == "0") {
            $("#ADMINnewimageassocForm").trigger("reset");
            $('.message-image').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
          }
          else if(data.error == "1") {
           $('.message-image').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
          }
        }
    });
  });

  $("#ADMINnewarticleimageForm").on("submit",function(e){
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    $.ajax({
        url : "/admin/newArticle",
        type: "POST",
        dataType: "json",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success:function(response){
          data = JSON.parse(JSON.stringify(response));
          if (data.error == "0") {
            $("#ADMINnewarticleimageForm").trigger("reset");
            $('.message-image').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
             var x = document.getElementById("afficher-images");
             x.innerHTML = "";
             data.images.forEach(display_images);
             if (data.images) {
               $('#associer').replaceWith('<span id="associer"><button class="btn btn-info btn-lg h-100 d-flex mx-auto align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">Associer image(s)</button></span>');
             }
          }
          else if(data.error == "1") {
           $('.message-image').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-image"></span>');
          }
        }
    });
  });

  $("#ADMINnewarticlemarqueForm").on("submit",function(e){
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        url : "/admin/newArticle",
        type: "POST",
        cache:false,
        dataType: "json",
        data: formData,
        success:function(response){
          data = JSON.parse(JSON.stringify(response));
          if (data.error == "0") {
            $("#ADMINnewarticlemarqueForm").trigger("reset");
            $('.message-marque').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-marque"></span>');
             var x = document.getElementById("ref_marques");
             x.innerHTML = "";
             x.innerHTML = '<option value="" selected disabled>Choississez une marque</option>';
             data.marques.forEach(display_marques);
          }
          else if(data.error == "1") {
           $('.message-marque').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-marque"></span>');
          }
        }
    });
  });

  $("#ADMINnewarticleForm").on("submit",function(e){
    e.preventDefault();
    var formData = $(this).serialize();
    $.ajax({
        url : "/admin/newArticle",
        type: "POST",
        cache:false,
        dataType: "json",
        data: formData,
        success:function(response){
          data = JSON.parse(JSON.stringify(response));
          if (data.error == "0") {
            $("#ADMINnewarticleForm").trigger("reset");
            $('.message-article').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-article"></span>');
             var x = document.getElementById("afficher-images");
             x.innerHTML = "";
             data.images.forEach(display_images);
             if (data.images.length == 0) {
               $('#associer').replaceWith('<span id="associer"><div class="alert alert-danger text-center">Il n\'y a pas d\'images disponibles. Ajoutez-en afin de pouvoir les associer.</div></span>');
             }
          }
          else if(data.error == "1") {
           $('.message-article').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
             + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-article"></span>');
          }
        }
    });
  });

});

</script>

<h2>Ajout d'article</h2>
<div class="container-fluid text-black">
  <div class="row border rounded">
    <div class="col-6 text-center">
      <h3 class="text-white text-center">Associer/Ajouter une ou plusieurs image(s)*</h3>
      <hr class="text-white">
      <span class="message-image"></span>
      <div class="row">
        <div class="col-6">
          <span id="associer">
            <?php if (empty($params['imagesdisponibles'])) {?>
              <div class="alert alert-danger text-center">
                Il n'y a pas d'images disponibles. Ajoutez-en afin de pouvoir les associer.
              </div>
            <?php } else { ?>
            <button class="btn btn-info btn-lg h-100 d-flex mx-auto align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasExample" aria-controls="offcanvasExample">
              Associer image(s)
            </button>
          <?php } ?>
        </span>
          <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasExample" aria-labelledby="offcanvasExampleLabel">
            <div class="offcanvas-header">
              <h3 class="offcanvas-title" id="offcanvasExampleLabel">Choississez une ou plusieurs image(s) pour le nouvel article</h3>
              <button type="button" class="btn-close" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
              <form action="/admin/newArticle" id="ADMINnewimageassocForm" method="post">
                <input type="submit" class="btn btn-info btn-lg w-100 mb-3" value="Confirmer choix des images" data-bs-dismiss="offcanvas">
                <div class="checkbox-group required">
                    <div class="form-check ps-0" id="afficher-images"></div>
                </div>
                <input type="submit" class="btn btn-info btn-lg w-100 mt-3" value="Confirmer choix des images" data-bs-dismiss="offcanvas">
              </form>
            </div>
          </div>
        </div>
        <div class="col-6 text-center border-start">
          <form action="/admin/newArticle" id="ADMINnewarticleimageForm" method="post" enctype="multipart/form-data">
              <input type="file" class="form-control form-control-lg mx-auto mb-3" name="path" id="path" required>
              <input type="submit" class="btn btn-info btn-lg w-100" value="Ajouter une nouvelle image">
          </form>
        </div>
      </div>
    </div>
    <div class="col-6 border-start">
      <h3 class="text-white text-center">Ajouter une marque*</h3>
      <hr class="text-white">
      <span class="message-marque"></span>
      <form action="/admin/newArticle" id="ADMINnewarticlemarqueForm" method="post">
        <div class="text-center">
          <div class="form-floating w-50 mx-auto">
            <input type="text" class="form-control form-control-lg mb-3" name="nom" id="nom" placeholder=" " required>
            <label for="nom">Encodez le nom de la nouvelle marque</label>
            <input type="submit" class="btn btn-info btn-lg w-100" value="Ajouter une nouvelle marque">
          </div>
        </div>
      </form>
    </div>
    <p class="text-center text-white">*Optionnel</p>
  </div>
  <hr>
  <div class="row border rounded">
    <h3 class="text-white text-center mt-3">Définir le nouvel article</h3>
    <hr class="text-white">
    <span class="message-article"></span>
    <form action="/admin/newArticle" id="ADMINnewarticleForm" method="post">
      <div class="row">
        <div class="col">
          <div class="form-floating mb-3">
            <input type="text" class="form-control form-control-lg" name="nom" id="nom" placeholder=" " required>
            <label for="nom">Nom du nouvel article</label>
          </div>
        </div>
        <div class="col">
          <div class="form-floating">
            <textarea class="form-control form-control-lg" name="description" id="description" placeholder=" " required></textarea>
            <label for="description">Description du nouvel article</label>
          </div>
        </div>
      </div>
      <div class="row mt-3">
        <div class="col">
          <div class="form-floating">
            <select class="form-select form-select-lg" name="ref_marques" id="ref_marques" required>
              <option value="" selected disabled>Choississez une marque</option>
            </select>
            <label for="ref_marques">Marque du nouvel article</label>
          </div>
        </div>
        <div class="col">
          <div class="form-floating mb-3">
            <input type="number" class="form-control form-control-lg" name="prix" id="prix" placeholder=" " min="1" max="1000" required>
            <label for="prix">Prix du nouvel article</label>
          </div>
        </div>
        <div class="col">
          <div class="form-floating">
            <select class="form-select form-select-lg" name="ref_categories" id="ref_categories" required>
              <option value="" selected disabled>Choississez une catégorie</option>
              <?php foreach ($params['categories'] as $categorie) { ?>
              <option value ="<?= $categorie->id ?>"> <?= $categorie->nom ?></option>
              <?php  } ?>
            </select>
            <label for="ref_categories">Catégorie du nouvel article</label>
          </div>
        </div>
      </div>
      <div class="row text-center">
        <div class="col">
          <input type="submit" class="btn btn-warning btn-lg mb-2 mt-4 w-25" value="Ajouter un nouvel article">
        </div>
      </div>
    </form>
  </div>
</div>
