<h2>Ajouter un nouveau type de réservations</h2>
<span class="message-message"></span>
<form action="/admin/newTypeReservation" id="ADMINnewtypeForm" method="post">
  <div class="container">
    <div class="row gy-5">
      <div class="col-6 text-center">
        <label for="nom">Nom :</label>
        <input type="text" class="form-control form-control-lg" name="nom" id="nom" required>
      </div>
      <div class="col-6 text-center">
        <label for="nombre_personnes">Nombre de personne(s) :</label>
        <input type="number" class="form-control form-control-lg" name="nombre_personnes" id="nombre_personnes" min="1" max="4" required>
      </div>
      <div class="col-6 text-center">
        <label for="prix">Prix Normal :</label>
        <input type="number" class="form-control form-control-lg" name="prix" id="prix" min="1" required>
      </div>
      <div class="col-6 text-center">
        <label for="prix_abonne">Prix Abonnés* : </label>
        <input type="number" class="form-control form-control-lg" name="prix_abonne" id="prix_abonne" min="1" required>
        <p>*doit être moins cher que le prix normal</p>
      </div>
    </div>
    <div class="row mt-4">
      <center><button type="submit" class="btn btn-light btn-lg">Ajouter un nouveau type de réservations</button></center>
    </div>
  </div>
</form>
