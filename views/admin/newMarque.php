<h2>Ajouter une nouvelle marque</h2>
<span class="message-message"></span>
<form action="/admin/newMarque" id="ADMINnewmarqueForm" method="post">
  <div class="container">
    <div class="row gy-5">
      <div class="col-4"></div>
      <div class="col-4 text-center">
        <label for="nom">Nom :</label>
        <input type="text" class="form-control form-control-lg" name="nom" id="nom" required>
      </div>
    </div>
    <div class="row mt-4">
      <center><button type="submit" class="btn btn-light btn-lg">Ajouter une nouvelle marque</button></center>
    </div>
  </div>
</form>
