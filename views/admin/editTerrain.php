<script type="text/javascript">
    var id = <?php Print($params['terrain']->id); ?>;
</script>

<h2>Modifier terrain #<?= $params['terrain']->id ?></h2>
<span class="message-message"></span>
<form action="/admin/editTerrain/<?= $params['terrain']->id ?>" id="ADMINeditterrainForm" method="post">
  <div class="container">
    <div class="row gy-5">
      <div class="col-4"></div>
      <div class="col-4 text-center">
        <label for="nom">Nom :</label>
        <input type="text" class="form-control form-control-lg" name="nom" id="nom" value="<?= $params['terrain']->nom?>" required>
        <label class="mt-2" for="ref_types_terrains">Type de terrain :</label>
        <select class="form-select form-select-lg" name="ref_types_terrains" id="ref_types_terrains" required>
          <option value="" selected disabled>Choississez un type de terrain</option>
          <?php foreach ($params['types_terrains'] as $types_terrains) { ?>
          <option <?php echo($params['terrain']->ref_types_terrains == $types_terrains->id) ? "selected" : ""; ?> value ="<?= $types_terrains->id ?>"> <?= $types_terrains->nom ?></option>
          <?php  } ?>
        </select>
      </div>
    </div>
    <div class="row mt-4">
      <center><button type="submit" class="btn btn-warning btn-lg">Modifier terrain</button></center>
    </div>
  </div>
</form>
