<h2>Ajouter une nouvelle image</h2>
<span class="message-message"></span>
<form action="/admin/newImage" id="ADMINnewimageForm" method="post" enctype="multipart/form-data">
  <div class="container">
    <div class="row gy-5">
      <div class="col-3"></div>
      <div class="col-6 text-center">
        <label for="path">Image :</label>
        <input type="file" class="form-control form-control-lg" name="path" id="path" required>
      </div>
    </div>
    <div class="row mt-4">
      <center><button type="submit" class="btn btn-light btn-lg">Ajouter une nouvelle image</button></center>
    </div>
  </div>
</form>
