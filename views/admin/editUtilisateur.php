<script type="text/javascript">
    var id = <?php Print($params['utilisateur']->id); ?>;
</script>


<h2>Modifier rôle/abonnement #<?= $params['utilisateur']->id ?></h2>
<span class="message-message"></span>
<form action="/admin/editUtilisateur/<?= $params['utilisateur']->id ?>" id="ADMINeditutilisateurForm" method="post">
  <div class="container">
    <div class="row gy-5">
      <div class="col-12 text-center">
        <label for="admin">Administrateur ? </label>
        <?php if ($params['utilisateur']->admin == 1) { ?>
          <input type="radio" class="btn-check" name="admin" id="admin0" value="0" autocomplete="off" required>
            <label class="btn btn-outline-info btn-lg" for="admin0">Non</label>
          <input type="radio" class="btn-check" name="admin" id="admin1" value="1" autocomplete="off" required checked>
            <label class="btn btn-outline-info btn-lg" for="admin1">Oui</label>
  <?php  }else{ ?>
          <input type="radio" class="btn-check" name="admin" id="admin0" value="0" autocomplete="off" required checked>
            <label class="btn btn-outline-info btn-lg" for="admin0">Non</label>
          <input type="radio" class="btn-check" name="admin" id="admin1" value="1" autocomplete="off" required>
            <label class="btn btn-outline-info btn-lg" for="admin1">Oui</label>
          <?php } ?>
      </div>
      <div class="col-12 text-center">
        <label for="admin">Abonné ? </label>
        <?php if ($params['utilisateur']->abonnement == 1) { ?>
          <input type="radio" class="btn-check" name="abonnement" id="abonnement0" value="0" autocomplete="off" required>
            <label class="btn btn-outline-info btn-lg" for="abonnement0">Non</label>
          <input type="radio" class="btn-check" name="abonnement" id="abonnement1" value="1" autocomplete="off" required checked>
            <label class="btn btn-outline-info btn-lg" for="abonnement1">Oui</label>
  <?php  }else{ ?>
          <input type="radio" class="btn-check" name="abonnement" id="abonnement0" value="0" autocomplete="off" required checked>
            <label class="btn btn-outline-info btn-lg" for="abonnement0">Non</label>
          <input type="radio" class="btn-check" name="abonnement" id="abonnement1" value="1" autocomplete="off" required>
            <label class="btn btn-outline-info btn-lg" for="abonnement1">Oui</label>
          <?php } ?>
      </div>
    </div>
    <div class="row mt-4">
      <center>
        <button type="submit" class="btn btn-warning btn-lg">Modifier rôle/abonnement</button>
      </center>
    </div>
  </div>
</form>
