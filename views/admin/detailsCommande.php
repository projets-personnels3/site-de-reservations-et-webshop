<script type="text/javascript">
    var id = <?php Print($params['commandes']->Id); ?>;
</script>

<h2>Détails commande #<?= $params['commandes']->Id ?></h2>
<span class="message-message"></span>
<?php $coutTotal=0; ?>
<div class="cadrevert rounded">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-9">
        <div class="container">
          <?php foreach ($params['commandes_articles'] as $commandes_articles): ?>
            <?php if ($params['commandes']->Id == $commandes_articles->Id) { ?>
              <div class="row mt-1">
                <div class="col-1"><?= $commandes_articles->Quantite ?> x</div>
                <div class="col-3"><img src="<?= $commandes_articles->Path ?>" width="120" height="120"></div>
                <div class="col-4"><?= $commandes_articles->Articles ?></div>
                <div class="col-1"><?= $commandes_articles->Tailles ?></div>
                <div class="col-3"><?= $commandes_articles->Cout ?>€</div>
              </div>
      <?php $coutTotal += $commandes_articles->Cout*$commandes_articles->Quantite; } ?>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="col-3">
        <form action="/admin/detailsCommande/<?= $params['commandes']->Id ?>" id="ADMINupdatestatutForm" method="post">
          <p><u>Commande n°</u> <b><?= $params['commandes']->Id ?></b></p>
          <p><i>Effectuée le</i> : <b><?php $date=date_create($params['commandes']->Dates);echo date_format($date,"d/m/Y"); ?></b></p>
          <p><i>Montant</i> : <b><?= $coutTotal ?>.00 €</b></p>
          <p><i>Statut</i> :
          <select class="form-select form-select-lg" name="ref_statuts">
      <?php foreach ($params['statuts'] as $statuts):
              if ($params['commandes']->Statuts == $statuts->nom)
              { ?>
                <option value="<?= $statuts->id ?>" selected><?= $statuts->nom ?></option>
        <?php }
              else
              { ?>
                <option value="<?= $statuts->id ?>"><?= $statuts->nom ?></option>
        <?php }
            endforeach ?>
          </select>
          </p>
          <p><button type="submit" class="btn btn-warning btn-lg" name="button">Modifier statut</button></p>
        </form>
      </div>
    </div>
  </div>
</div>
