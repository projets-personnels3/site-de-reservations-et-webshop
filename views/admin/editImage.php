<script type="text/javascript">
    var id = <?php Print($params['image']->id); ?>;
</script>

<h2>Modifier image #<?= $params['image']->id ?></h2>
<span class="message-message"></span>
<form action="/admin/editImage/<?= $params['image']->id ?>" id="ADMINeditimageForm" method="post" enctype="multipart/form-data">
  <div class="container">
    <div class="row gy-5">
      <div class="col-3"></div>
      <div class="col-6 text-center">
        <label for="path">Image :</label>
        <input type="file" class="form-control form-control-lg" name="path" id="path" required>
      </div>
    </div>
    <div class="row mt-4">
      <center><button type="submit" class="btn btn-warning btn-lg">Modifier image</button></center>
    </div>
  </div>
</form>
