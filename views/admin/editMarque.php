<script type="text/javascript">
    var id = <?php Print($params['marque']->id); ?>;
</script>

<h2>Modifier marque #<?= $params['marque']->id ?></h2>
<span class="message-message"></span>
<form action="/admin/editMarque/<?= $params['marque']->id ?>" id="ADMINeditmarqueForm" method="post">
  <div class="container">
    <div class="row gy-5">
      <div class="col-4"></div>
      <div class="col-4 text-center">
        <label for="nom">Nom :</label>
        <input type="text" class="form-control form-control-lg" name="nom" id="nom" value="<?= $params['marque']->nom?>" required>
      </div>
    </div>
    <div class="row mt-4">
      <center><button type="submit" class="btn btn-warning btn-lg">Modifier marque</button></center>
    </div>
  </div>
</form>
