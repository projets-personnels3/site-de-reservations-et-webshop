<script type="text/javascript">
    var id = <?php Print($params['image']->id); ?>;
</script>

<h2>Associer l'image #<?= $params['image']->id ?> à un article</h2>
<span class="message-message"></span>
<form action="/admin/imageArticle/" id="ADMINimagearticleForm" method="post">
  <input type="hidden" name="ref_images" value="<?= $params['image']->id ?>">
  <div class="container">
    <div class="row gy-5">
      <div class="col-3"><img src="<?= $params['image']->path ?>" alt="image" width="400" height="400"></div>
      <div class="col-3"></div>
      <div class="col-6 text-center">
        <label for="ref_articles">Articles :</label>
          <select class="form-select" name="ref_articles" id="ref_articles" aria-label="ref_articles" required>
            <option selected disabled>Choississez un article à associer à l'image</option>
              <?php foreach ($params['articles'] as $articles) { ?>
                <option value="<?= $articles->id ?>"><?= $articles->nom ?></option>
            <?php  } ?>
          </select>
      </div>
    </div>
    <div class="row mt-4">
      <center><button type="submit" class="btn btn-warning btn-lg">Associer l'image à l'article</button></center>
    </div>
  </div>
</form>
