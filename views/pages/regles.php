<h2>Règles du jeu de padel</h2>
  <div class="conteneurTexte">
    Le padel est un sport de raquettes qui se pratique uniquement en double, en extérieur comme en salle. Le terrain est clos, fermé par des vitrages surmontés de grillages en fond de court, et de grillages sur les côtés. Un filet sépare comme au tennis les joueurs de chaque équipe. Le terrain mesure 20m x 10m, les parois mesurent 4 mètres de haut sur les fonds de court et 3m sur les côtés du terrain.
  </div>
  <br>
    <center><img src="../ressources/Images/terrain-padel.jpg" alt="Dimensions d'un terrain de padel"></center>
    <br>
  <div class="conteneurTexte">
    Le jeu s’organise comme au tennis, les parois font figure de lignes de fond de court. Si la balle les frappe directement durant le jeu avant de toucher le terrain, elle est faute. Si elle touche le sol dans le camp adverse pour frapper une paroi ensuite, elle est toujours en jeu ! Il ne faut pas qu’elle rebondisse une deuxième fois au sol. Durant l’échange, les joueurs peuvent frapper la balle à la volée ou la remettre dans le court depuis l’extérieur !<br><br>
  </div>
  <div class="conteneurSousTitre">
    Les points :
  </div>
  <div class="conteneurTexte">
    • Le padel emprunte au tennis son système de points.<br>
    • Il faut 6 jeux pour gagner un set et l’équipe qui gagne 2 sets remporte la partie.<br>
    • Un jeu se décompose exactement comme au tennis 15/0, 30/0, 40/0, avantage, égalité, etc…<br>
    • En cas d’égalité à 6/6, on procède au jeu décisif comme au tennis.<br><br>
  </div>
  <div class="conteneurSousTitre">
    Le service :
  </div>
  <div class="conteneurTexte">
    • La mise en jeu se fait en diagonale comme au tennis, avec 2 tentatives de services (première et deuxième balle).<br>
    • Le serveur est derrière la ligne de service, de son côté de la diagonale.<br>
    • Le receveur se place où il veut sur le terrain, dans la diagonale du serveur.<br>
    • Le serveur fait d’abord rebondir la balle puis la frappe sous la ceinture. La balle et le serveur sont derrière la ligne.<br>
    • La balle doit rebondir dans le carré de service opposé avant d’être frappée par le receveur.<br>
    • Si la balle ne rebondit pas dans le carré, reste dans le filet, ou heurte une paroi avant le rebond elle est faute.<br>
    • Si elle rebondit dans le carré et touche le grillage ensuite elle est faute.<br>
    • La balle peut toucher le vitrage après le rebond dans le carré de service.<br>
    • Si la balle frappe le filet et rebondit dans le carré elle est let, on remet la mise en jeu.<br><br>
  </div>
  <div class="conteneurSousTitre">
    Le jeu :
  </div>
  <div class="conteneurTexte">
    • Une fois la balle en jeu, toutes les balles qui passent le filet doivent d’abord rebondir sur le sol du camp adverse avant de toucher une paroi.<br>
    • On peut frapper la balle à la volée.<br>
    • Les joueurs peuvent frapper la balle après un rebond sur une paroi pour la renvoyer dans le camp adverse.<br>
    • On peut frapper la balle pour qu’elle rebondisse sur la partie vitrée de notre propre camp afin de la faire passer de l’autre côté du filet.<br>
    • Bien sûr comme au tennis, la balle ne peut rebondir qu’une seule fois dans votre camp, et elle ne peut être frappée qu’une fois.<br>
    • On peut smasher la balle assez fort pour qu’après le rebond elle sorte des limites du court !<br>
    • Mais l’équipe adverse pourra courir la chercher et tenter de la remettre en jeu.<br><br>
  </div>
  <div class="conteneurSousTitre">
    En résumé, quand est-ce qu’on perd le point ?
  </div>
  <div class="conteneurTexte">
    • Si on laisse rebondir la balle deux fois.<br>
    • Si on rate 2 fois son service (dans le filet, en dehors du carré, contre le grillage).<br>
    • Si on frappe directement contre la vitre ou le grillage de l’adversaire.<br>
    • Si on renvoie la balle chez son adversaire avec l’aide de son propre grillage.<br>
    • Si on touche le filet, le poteau du filet ou toute autre chose du côté de son adversaire avec le corps ou la raquette.<br>
  </div>
