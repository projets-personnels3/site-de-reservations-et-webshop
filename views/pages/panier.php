<h2>Panier</h2>
<span class="message-message"></span>
<?php if ($params['panier'] == NULL) { ?>
  <center><div class="alert alert-danger" role="alert">
  Votre panier est vide
</div></center>
<?php  }
else { ?>
<div class="container-fluid">
  <div class="row">
    <div class="col-9">
      <div class="table-responsive">
        <table class="table table-hover align-middle">
        <thead>
          <tr>
            <th scope="col">Articles</th>
            <th scope="col">Taille</th>
            <th scope="col">Quantité</th>
            <th scope="col">Prix</th>
            <th scope="col">Ajouter</th>
            <th scope="col">Enlever</th>
            <th scope="col">Supprimer</th>
          </tr>
        </thead>
        <tbody>
<?php $coutTotal=0; ?>
<?php foreach ($params['panier'] as $panier) { ?>
          <tr>
            <td><img src="<?= $panier->path ?>" width="100" height="100">    <?= $panier->nom ?></td>
            <td><?= $panier->tailles ?></td>
            <td>x <?= $panier->quantite ?></td>
            <td><?= $panier->cout ?>€</td>
            <td>
              <form action="/ajouterPanier/<?= $panier->id ?>" method="post">
                <button type="submit" class="btn btn-warning btn-lg"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cart-plus" viewBox="0 0 16 16">
  <path d="M9 5.5a.5.5 0 0 0-1 0V7H6.5a.5.5 0 0 0 0 1H8v1.5a.5.5 0 0 0 1 0V8h1.5a.5.5 0 0 0 0-1H9V5.5z"/>
  <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
</svg></button>
              </form>
            </td>
            <td>
              <form action="/enleverPanier/<?= $panier->id ?>" method="post">
                <button type="submit" class="btn btn-warning btn-lg"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cart-dash" viewBox="0 0 16 16">
  <path d="M6.5 7a.5.5 0 0 0 0 1h4a.5.5 0 0 0 0-1h-4z"/>
  <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zm3.915 10L3.102 4h10.796l-1.313 7h-8.17zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
</svg></button>
              </form>
            </td>
            <td>
              <form action="/supprimerDuPanier/<?= $panier->id ?>" method="post">
                <button type="submit" class="btn btn-danger btn-lg"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cart-x-fill" viewBox="0 0 16 16">
                <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM7.354 5.646 8.5 6.793l1.146-1.147a.5.5 0 0 1 .708.708L9.207 7.5l1.147 1.146a.5.5 0 0 1-.708.708L8.5 8.207 7.354 9.354a.5.5 0 1 1-.708-.708L7.793 7.5 6.646 6.354a.5.5 0 1 1 .708-.708z"/>
                </svg></button>
              </form>
            </td>
          </tr>
<?php $coutTotal += $panier->cout*$panier->quantite; } ?>
        </tbody>
        </table>
      </div>
      </div>
      <div class="col-1"></div>
      <div class="col-2 cadreblanc rounded text-center h-50">
        <h3>Prix total</h3>
        <hr style="color:#198754">
        <h3><?= $coutTotal ?>.00 €</h3>
        <form action="/panier" method="post">
          <input type="hidden" name="ref_utilisateurs" value="<?= $_SESSION['IdUtilisateurAuth'] ?>">
          <input type="hidden" name="ref_statuts" value="1">
  <?php   $now = new DateTime();
          $date = $now->format('Y\-m\-d\ h:i:s');?>
          <input type="hidden" name="dates" value="<?= $date ?>">
          <hr style="color:#198754">
          <button type="submit" class="btn btn-success">Commander</button>
        </form>
      </div>
  </div>
</div>
<?php }  ?>
