<h2>Mes commandes</h2>
<?php if (($params['commandes']) == NULL) { ?>
  <center><div class="alert alert-danger" role="alert">Vous n'avez pas de commandes !</div></center>
<?php }else {
foreach ($params['commandes'] as $commandes):
 $coutTotal=0; ?>
<div class="cadrevert rounded">
  <div class="container-fluid">
    <div class="row mt-4">
      <div class="col-9">
        <div class="container">
          <?php foreach ($params['commandes_articles'] as $commandes_articles): ?>
            <?php if ($commandes->Id == $commandes_articles->Id) { ?>
              <a class="liensCommandesArticles" href="/article/<?= $commandes_articles->IdArticles ?>">
              <div class="row mt-1">
                <div class="col-1"><?= $commandes_articles->Quantite ?> x</div>
                <div class="col-3"><img src="<?= $commandes_articles->Path ?>" width="120" height="120"></div>
                <div class="col-4"><?= $commandes_articles->Articles ?></div>
                <div class="col-1"><?= $commandes_articles->Tailles ?></div>
                <div class="col-3"><?= $commandes_articles->Cout ?>€</div>
              </div></a>
      <?php $coutTotal += $commandes_articles->Cout*$commandes_articles->Quantite; } ?>
          <?php endforeach; ?>
        </div>
      </div>
      <div class="col-3">
        <p><u>Commande n°</u> <b><?= $commandes->Id ?></b></p>
        <p><i>Statut</i> : <b><?= $commandes->Statuts ?></b></p>
        <p><i>Effectuée le</i> : <b><?php $date=date_create($commandes->Dates);echo date_format($date,"d/m/Y"); ?></b></p>
        <p><i>Montant</i> : <b><?= $coutTotal ?>.00 €</b></p>
        <form action="/deleteCommande/<?= $commandes->Id ?>" method="post" class="d-inline">
          <button type="submit" class="btn btn-danger" onclick="return confirm('Êtes-vous sûr de vouloir annuler votre commande ?')">Annuler la commande</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach; ?>
<?php } ?>
