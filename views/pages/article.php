<span class="message-message"></span>
<br>
<div class="container">
  <div class="row">
    <div class="col-5">
      <div id="carouselExampleIndicators" class="carousel slide carousel-dark carousel-fade" data-bs-ride="carousel">
        <div class="carousel-inner rounded">
          <?php $compteur_image = 0 ?>
          <?php foreach ($params['images'] as $images): ?>
            <div class="carousel-item <?= ($compteur_image === 0) ? "active" : ""; ?>">
                  <img src="<?= $images->path ?>" class="card-img-top" alt="Erreur chargement d'image">
            </div>
            <?php $compteur_image++ ?>
          <?php endforeach; ?>
        </div>
        <?php if ($compteur_image >= 2) { ?>
          <button class="carousel-control-prev rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        <?php  } ?>
      </div>
    </div>
    <div class="col-2"></div>
    <div class="col-5">
      <div class="cadreblanc rounded">
        <h3><?= $params['article']->nom ?></h3>
        <h4><?php foreach ($params['marques'] as $marques) {if ($params['article']->ref_marques == $marques->id) {echo $marques->nom."®";}} ?></h4>
        <hr class="text-success border-6 opacity-75">
        <p><?= $params['article']->description ?></p>
        <div class="text-center">
<form action="/ajouterAuPanier" id="ajouteraupanierForm" method="post">
        <?php if ($params['article']->ref_categories == 2) {?>
          <select class="form-select form-select-lg" name="tailles" required>
            <option value="" disabled selected>--Choisissez une taille--</option>
            <option value="XS">XS</option>
            <option value="S">S</option>
            <option value="M">M</option>
            <option value="L">L</option>
            <option value="XL">XL</option>
          </select>
        <?php   } ?>
        <?php if (!isset($_SESSION['IdUtilisateurAuth'])) {
          $_SESSION['IdUtilisateurAuth'] = 0;
        } ?>
        <?php if ($params['article']->ref_categories == 3) {?>
          <select class="form-select form-select-lg" name="tailles" required>
            <option value="" disabled selected>--Choisissez une pointure--</option>
            <option value="36">36</option>
            <option value="37">37</option>
            <option value="38">38</option>
            <option value="39">39</option>
            <option value="40">40</option>
            <option value="41">41</option>
            <option value="42">42</option>
            <option value="43">43</option>
            <option value="44">44</option>
            <option value="45">45</option>
            <option value="46">46</option>
          </select>
        <?php   } ?>
      <br>
            <input type="hidden" name="ref_articles" value="<?= $params['article']->id ?>">
            <input type="hidden" name="ref_utilisateurs" value="<?= $_SESSION['IdUtilisateurAuth'] ?>">
            <input type="hidden" name="quantite" value="1">
            <?php if ($params['article']->ref_categories == 1 || $params['article']->ref_categories == 4) {?>
            <input type="hidden" name="tailles" value="/">
            <input type="hidden" name="cout" value="<?= $params['article']->prix ?>">
            <?php   } ?>
            <?php if(!isset($_SESSION['auth']))
                  { ?>
              <div class="alert alert-danger" role="alert">Il faut être connecté pour ajouter un article au panier !</div>
            <?php } else { ?>
              <button type="submit" class="btn btn-success btn-lg">Ajouter au panier <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-cart-plus-fill" viewBox="0 0 16 16">
                <path d="M.5 1a.5.5 0 0 0 0 1h1.11l.401 1.607 1.498 7.985A.5.5 0 0 0 4 12h1a2 2 0 1 0 0 4 2 2 0 0 0 0-4h7a2 2 0 1 0 0 4 2 2 0 0 0 0-4h1a.5.5 0 0 0 .491-.408l1.5-8A.5.5 0 0 0 14.5 3H2.89l-.405-1.621A.5.5 0 0 0 2 1H.5zM6 14a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm7 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zM9 5.5V7h1.5a.5.5 0 0 1 0 1H9v1.5a.5.5 0 0 1-1 0V8H6.5a.5.5 0 0 1 0-1H8V5.5a.5.5 0 0 1 1 0z"/>
              </svg></button>
            <?php  } ?>
            <button type="button" class="btn btn-warning btn-lg"><?= $params['article']->prix ?> €</button>
          </form>
        </div>
      </div>
      </div>
    </div>
  </div>
