<div class="container">
  <div class="row">
    <div class="col-2"></div>
    <div class="col-8">
      <div id="carouselExampleIndicators" class="carousel carousel-dark slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Reservation"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Boutique"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Regles"></button>
          <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="3" aria-label="Faq"></button>
        </div>
        <div class="carousel-inner rounded">
          <div class="carousel-item active">
            <a href="/reserver">
            <img src="../../ressources/images/carousel-reserver.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block rounded">
              <h5 class="textecarousel rounded">Réserver un terrain</h5>
            </div>
            </a>
          </div>
          <div class="carousel-item">
            <a href="/regles">
            <img src="../../ressources/images/carousel-regles.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block rounded">
              <h5 class="textecarousel rounded">Se renseigner sur les règles</h5>
            </div>
            </a>
          </div>
          <div class="carousel-item">
            <a href="/boutique">
            <img src="../../ressources/images/carousel-boutique.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block rounded">
              <h5 class="textecarousel rounded">S'acheter du matériel</h5>
            </div>
            </a>
          </div>
          <div class="carousel-item">
            <a href="/faq">
            <img src="../../ressources/images/carousel-faq.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block rounded">
              <h5 class="textecarousel rounded">Des questions ? Jetez un oeil à notre FAQ !</h5>
            </div>
            </a>
          </div>
        </div>
          <button class="carousel-control-prev rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button class="carousel-control-next rounded" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </div>
      <div class="col-2"></div>
  </div>
</div>
