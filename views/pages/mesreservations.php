<h2>Mes réservations</h2>
<?php if (($params['reservations']) == NULL) { ?>
  <center><div class="alert alert-danger" role="alert">Vous n'avez pas de réservations !</div></center>
<?php }else { ?>
  <table class="table table-hover table-bordered">
    <thead>
      <tr class="table-light">
        <th scope="col">#</th>
        <th scope="col">Date</th>
        <th scope="col">Types Terrains</th>
        <th scope="col">Terrain</th>
        <th scope="col">Créneau</th>
        <th scope="col">Types Réservations</th>
        <th scope="col">Nombre de personne(s)</th>
        <th scope="col">Coût</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $hier = new DateTime("now");
      $hier->modify('-1 day');
      foreach ($params['reservations'] as $reservation){
        if ($date=date_create($reservation->Dates) < $hier) { ?>
          <tr class="table-danger">
      <?php  }else { ?>
        <tr>
    <?php  }  ?>
    <td><?= $reservation->Id ?></td>
    <td><?php $date=date_create($reservation->Dates); echo date_format($date,"d/m/Y"); ?></td>
    <td><?= $reservation->Types_terrains ?></td>
    <td style="word-break: break-all;"><?= $reservation->Terrain ?></td>
    <td><?= $reservation->Creneau ?></td>
    <td style="word-break: break-all;"><?= $reservation->Types_reservations ?></td>
    <td><?= $reservation->Nombre_personnes ?></td>
    <td><?= $reservation->Cout ?> €</td>
  <?php
  if ($date=date_create($reservation->Dates) > $hier) { ?>
      <td><a href="/editReservation/<?= $reservation->Id ?>" class="btn btn-warning">Modifier</a>
          <form action="/deleteReservation/<?= $reservation->Id ?>" method="post" class="d-inline">
            <button type="submit" class="btn btn-danger">Annuler</button></form>
      </td>
  <?php  }else { ?>
    <td>Réservation passée</td>
  <?php  }  ?>

    </tr>
  <?php }?>
    </tbody>
  </table>
<?php } ?>
