<?php $articles_json = json_encode($params['articles']) ?>

<script type="text/javascript">

const articles = <?php print $articles_json;?> ;
var resultats = articles;

window.addEventListener('load', function () {
  resultats.forEach(display);
});

function display(item) {
 document.getElementById('articles-js').innerHTML += '<div class="col-3"><div class="card text-bg-light mb-2 border border-success"><div class="card-body text-center"><a href="/article/'+item.Id+'"><img src="'+item.Image+'" alt="Pas d\'image disponible pour cet article" style="width: 200px; height: 200px;"></a><hr class="text-success border-6 opacity-75"><h3 class="card-title">'+item.Marque+' ®</h3><hr class="text-success border-6 opacity-75"><a class="btn btn-lg btn-warning" style="width:100%" href="/article/'+item.Id+'">'+item.Prix+' €</a></div></div></div>'
}


const functionTriPrix = (a,b) => {
  let diff = a.Prix - b.Prix;
  if (diff<0) {
    return -1;
  }
  else {
    return 1;
  }
}

const functionTriPrixInverse = (a,b) => {
  let diff = b.Prix - a.Prix;
  if (diff<0) {
    return -1;
  }
  else {
    return 1;
  }
}

const functionTriMarques = (a,b) => {
  if (a.Marque < b.Marque) {
    return -1;
  }
  else {
    return 1;
  }
}

const functionTriMarquesInverse = (a,b) => {
  if (b.Marque < a.Marque) {
    return -1;
  }
  else {
    return 1;
  }
}

function tri(selectObject){
  var value = selectObject.value;

  if (value == 1) {
    resultats = articles.sort(functionTriPrix);
    var x = document.getElementById("articles-js");
    x.innerHTML = "";
    resultats.forEach(display);
  }
  else if (value == 2) {
    resultats = articles.sort(functionTriPrixInverse);
    var x = document.getElementById("articles-js");
    x.innerHTML = "";
    resultats.forEach(display);
  }
  else if (value == 3) {
    resultats = articles.sort(functionTriMarques);
    var x = document.getElementById("articles-js");
    x.innerHTML = "";
    resultats.forEach(display);
  }
  else if (value == 4) {
    resultats = articles.sort(functionTriMarquesInverse);
    var x = document.getElementById("articles-js");
    x.innerHTML = "";
    resultats.forEach(display);
  }
}

function filtreCategories(selectObject){
  var value = selectObject.value;
  resultats = articles.filter(valeur => {if(valeur.RefCategories === value) return true;})
  var x = document.getElementById("articles-js");
  x.innerHTML = "";
  resultats.forEach(display);
}

function filtreMarques(selectObject){
  var value = selectObject.value;
  resultats = articles.filter(valeur => {if(valeur.RefMarques === value) return true;})
  var x = document.getElementById("articles-js");
  x.innerHTML = "";
  resultats.forEach(display);
}

</script>


<h2>Boutique</h2>
<div class="container-fluid">
  <div class="row">
  <div class="col-3">
    <select class="form-select form-select-lg" id="tri" onchange="tri(this)">
      <option value="" disabled selected>--Trier les articles--</option>
      <option value="1">Trier les articles par prix croissant</option>
      <option value="2">Trier les articles par prix décroissant</option>
      <option value="3">Trier les articles par ordre alphabétique des marques</option>
      <option value="4">Trier les articles par ordre alphabétique inverse des marques</option>
    </select>
    <div class="card text-bg-light p-3 mt-1">
      <h3 class="text-center"><u>Filtrer par catégories</u></h3>
      <?php foreach ($params['categories'] as $categories):?>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="categories" id="<?= $categories->nom ?>" onclick="filtreCategories(this)" value="<?= $categories->id ?>">
          <label class="form-check-label" for="<?= $categories->nom ?>"><?= $categories->nom ?></label>
        </div>
      <?php endforeach ?>
      <hr class="text-success border-6 opacity-75">
      <h3 class="text-center"><u>Filtrer par marques</u></h3>
      <?php foreach ($params['marques'] as $marques):?>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="marques" id="<?= $marques->nom ?>" onclick="filtreMarques(this)" value="<?= $marques->id ?>">
          <label class="form-check-label" for="<?= $marques->nom ?>"><?= $marques->nom ?></label>
        </div>
      <?php endforeach ?>
    </div>
  </div>
  <div class="col-9">
      <div class="row text-justify" id="articles-js">
    </div>
  </div>
</div>
</div>
