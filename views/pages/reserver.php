<?php $reservations_json = json_encode($params['reservations']) ?>
<?php $horaires_json = json_encode($params['horaires']) ?>
<?php $terrains_json = json_encode($params['terrains']) ?>
<?php $types_reservations_json = json_encode($params['types_reservations']) ?>
<?php $types_terrains_json = json_encode($params['types_terrains']) ?>

<script type="text/javascript">
	var ref_utilisateurs = <?php echo $_SESSION["IdUtilisateurAuth"]; ?>;
	var abonnement = <?php echo $_SESSION["abonnement"]; ?>;
	var reservations_faites = <?php print $reservations_json;?> ;
	var horaires = <?php print $horaires_json;?> ;
	var terrains = <?php print $terrains_json;?> ;
	var types_reservations = <?php print $types_reservations_json;?> ;
	var types_terrains = <?php print $types_terrains_json;?> ;
	var aujourdhui = new Date();
	aujourdhui.setHours(0,0,0,0);
	var semaine = new Date(aujourdhui);
	semaine.setDate(semaine.getDate()+ 14);
	var semaine_abonne = new Date(aujourdhui);
	semaine_abonne.setDate(semaine_abonne.getDate()+ 16);

	(function($) {

		"use strict";

		// Initialise le calendrier avec la date actuelle
	$(document).ready(function(){
	    var date = new Date();
	    // Initialise les boutons du calendrier
	    $(".year-next-button").click({date: date}, next_year);
	    $(".year-prev-button").click({date: date}, prev_year);
			$(".month-next-button").click({date: date}, next_month);
	    $(".month-prev-button").click({date: date}, prev_month);
	    init_calendar(date);
	});

	// Fonction de l'affichage du calendrier
	function init_calendar(date) {
	    $(".tbody").empty();
			$(".reservations-container").empty();
	    var calendar_days = $(".tbody");
	    var month = date.getMonth();
	    var year = date.getFullYear();
	    var day_count = days_in_month(month, year);
	    var row = $("<tr class='table-row'></tr>");
	    var today = date.getDate();
	    // Set la date à 1 pour trouver le premier jour du mois
			date.setDate(1);
	    var first_day = date.getDay();
	    // 35+firstDay est le nombre d'éléments de date à ajouter à la table des dates
	    // 35 vient de (7 jour dans une semaine) * (jusqu'à 5 semaines dans un mois)
	    for(var i=0; i<35+first_day; i++) {
	        // Étant donné que certains éléments seront vides,
	        // il faut calculer la date réelle à partir de l'index
	        var day = i-first_day+1;
	        // si c'est un dimanche, il faut aller à la ligne
	        if(i%7===1) {
	            calendar_days.append(row);
	            row = $("<tr class='table-row'></tr>");
	        }
					var comparer_date = new Date(year, month, day);
	        // si l'index actuel n'est pas un jour de ce mois, il doit être vide
	        if(i < first_day || day > day_count) {
	            var curr_date = $("<td class='table-date nil'>"+"</td>");
	            row.append(curr_date);
	        }
	        else {
	            var curr_date = $("<td class='table-date'>"+day+"</td>");
							if (abonnement == 0) {
								if($(".active-date").length===0 && aujourdhui.getTime() == comparer_date.getTime()) {
										curr_date.addClass("active-date");
										var dates =  new Date(year, month, day);
										show_reservations(dates);
								}
								else if (aujourdhui.getTime() > comparer_date.getTime() || semaine.getTime() < comparer_date.getTime()) {
									var curr_date = $("<td class='table-date nil'>"+day+"</td>");
									curr_date.addClass("blocked-date");
								}
							}
							else if (abonnement == 1) {
								if($(".active-date").length===0 && aujourdhui.getTime() == comparer_date.getTime()) {
										curr_date.addClass("active-date");
										var dates =  new Date(year, month, day);
										show_reservations(dates);
								}
								else if (aujourdhui.getTime() > comparer_date.getTime() || semaine_abonne.getTime() < comparer_date.getTime()) {
									var curr_date = $("<td class='table-date nil'>"+day+"</td>");
									curr_date.addClass("blocked-date");
								}
							}

	            // Définir le gestionnaire onClick pour cliquer sur une date
							var dates =  new Date(year, month, day);
	            curr_date.click({dates:dates}, date_click);
	            row.append(curr_date);
	        }
	    }
	    // Ajouter la dernière ligne et définir l'année en cours
	    calendar_days.append(row);
	    $(".year").text(year);
			$(".month").text(mois[month]);
	}

	// Obtenir le nombre de jours dans un mois/année donné
	function days_in_month(month, year) {
	    var monthStart = new Date(year, month, 1);
	    var monthEnd = new Date(year, month + 1, 1);
	    return (monthEnd - monthStart) / (1000 * 60 * 60 * 24);
	}

	// gestionnaire d'événements pour quand une date est cliquée
	function date_click(donnee) {
		var date = donnee.data.dates;
		if (aujourdhui.getTime() <= date.getTime() && semaine.getTime() >= date.getTime()) {
			$(".active-date").removeClass("active-date");
	    $(this).addClass("active-date");
			show_reservations(date);
		}
	};

	// gestionnaire d'événements lorsque le bouton droit de l'année est cliqué
	function next_month(donnee) {
	    var date = donnee.data.date;
	    var new_month = date.getMonth()+1;
	    $("month").html(new_month);
	    date.setMonth(new_month);
	    init_calendar(date);
	}

	// gestionnaire d'événements lorsque le bouton gauche de l'année est cliqué
	function prev_month(donnee) {
			var date = donnee.data.date;
			var new_month = date.getMonth()-1;
			$("month").html(new_month);
			date.setMonth(new_month);
			init_calendar(date);
	}

	// gestionnaire d'événements lorsque le bouton droit de l'année est cliqué
	function next_year(donnee) {
	    var date = donnee.data.date;
	    var new_year = date.getFullYear()+1;
	    $("year").html(new_year);
	    date.setFullYear(new_year);
	    init_calendar(date);
	}

	// gestionnaire d'événements lorsque le bouton gauche de l'année est cliqué
	function prev_year(donnee) {
	    var date = donnee.data.date;
	    var new_year = date.getFullYear()-1;
	    $("year").html(new_year);
	    date.setFullYear(new_year);
	    init_calendar(date);
	}

	function show_reservations(dates) {
    $(".reservations-container").empty();
		dates.setHours(0,0,0,0);
		if (aujourdhui.getTime() <= dates.getTime() && semaine.getTime() >= dates.getTime()) {
			$(".reservations-container").show();
			var reservation_jour = [];
			reservations_faites.forEach((item) => {
				var test_dates = new Date(item.dates);
				test_dates.setHours(0,0,0,0);
				if (test_dates.getTime() == dates.getTime()) {
					 reservation_jour.push(item);
				}
			});

			var container = $('<div class="container-fluid h-100"></div>');
			$(".reservations-container").append(container);

			var date_reservation;
			date_reservation = new Date(dates);
			date_reservation = date_reservation.getFullYear() + '-' +
			('00' + (date_reservation.getMonth()+1)).slice(-2) + '-' +
			('00' + date_reservation.getDate()).slice(-2);
			var dateString = dates.toLocaleDateString();

			types_terrains.forEach((type_terrain) =>{
				//affiche boutons types terrains
				var row = $('<div class="row h-50 border-bottom"></div>');
				var form =$('<form class="p-0" action="/reserver" method="post"></form>');
				row.append(form);
				container.append(row);
				var boutons_types_terrains = $('<button type="button" id="bouton-'+ type_terrain.id +'" class="transition btn btn-'+ (type_terrain.id % 2 ? 'light':'success') +' w-100 fs-3" data-bs-toggle="button">Afficher les horaires des terrains en '+ type_terrain.nom +'</button>');
				form.append(boutons_types_terrains);
				//affiche liste terrains
				var afficher_terrains = "";
				terrains.forEach((terrain) => {
					if (terrain.ref_types_terrains == type_terrain.id) {
						afficher_terrains += '<option value="'+ terrain.id +'">'+ terrain.nom +'</option>';
					}
				});
				var select_terrains = $('<select class="form-select mt-3" name="ref_terrains" id="select-terrains-'+ type_terrain.id +'" required><option value="" selected disabled>Choississez un terrain</option></select>');
				var conteneur_terrains = $('<div class="col-11 mx-auto" id="terrains-'+ type_terrain.id +'" style="display : none;"></div>');
				select_terrains.append(afficher_terrains);
				conteneur_terrains.append(select_terrains);
				form.append(conteneur_terrains);
				terrains.forEach((terrain) => {
					horaires.forEach((item) => {
					item.disponible = "true";
					});
					var afficher_horaires = "";
					if (terrain.ref_types_terrains == type_terrain.id) {
						//affiche liste creneaux pour chaque terrain selon son type
						horaires.forEach((horaire) => {
							if (reservation_jour.length != 0) {
								reservation_jour.forEach((reservation) => {
									if (reservation.ref_horaires == horaire.id && reservation.ref_terrains == terrain.id) {
										horaire.disponible = "false";
									}
								});
							}
							if (horaire.disponible == "true") {
								afficher_horaires += '<input checked type="radio" class="btn-check" name="ref_horaires" id="creneaux-'+ terrain.id +'-'+ horaire.id +'" value="'+ horaire.id +'" required><label class="btn btn-'+ (type_terrain.id % 2 ? 'light':'success') +' mb-1 ms-1" for="creneaux-'+ terrain.id +'-'+ horaire.id +'">'+ horaire.creneaux +'</label>';
							}
							else {
								afficher_horaires += '<input disabled type="radio" class="btn-check" name="ref_horaires" id="creneaux-'+ terrain.id +'-'+ horaire.id +'" value="'+ horaire.id +'" required><label class="btn btn-danger mb-1 ms-1" for="creneaux-'+ terrain.id +'-'+ horaire.id +'">'+ horaire.creneaux +'</label>';
							}
						});
						var conteneur_horaires = $('<div class="horaires mt-3" id="horaires-'+ terrain.id +'" style="display : none;"></div>');
						conteneur_horaires.append(afficher_horaires);
						form.append(conteneur_horaires);
					}
				});
				//affiche select type reservation
				var afficher_types_reservations = "";
				var row_fin = $('<div class="row mt-3"></div>');
				types_reservations.forEach((type_reservation) => {
					afficher_types_reservations += '<option value="'+ type_reservation.id +'">'+ type_reservation.nom +' | '+ type_reservation.nombre_personnes +' personne(s) | '+ (abonnement == 0 ? type_reservation.prix +' €' : type_reservation.prix_abonne +' €' ) +'</option>';
				});
				var select_types_reservations = $('<select class="form-select" name="ref_types_reservations" id="select-types_reservations-'+ type_terrain.id +'" required><option value="" selected disabled>Choississez un type de réservation</option></select>');
				var conteneur_types_reservations = $('<div class="col-7 ms-3" id="types_reservations-'+ type_terrain.id +'" style="display : none;"></div>');
				select_types_reservations.append(afficher_types_reservations);
				conteneur_types_reservations.append(select_types_reservations);
				row_fin.append(conteneur_types_reservations);
				form.append(row_fin);
				//affiche bouton final
				var boutons_finaux = $('<input type="hidden" name="ref_utilisateurs" value="'+ ref_utilisateurs +'"><input type="hidden" name="dates" value="'+ date_reservation +'"><input type="submit" class="btn btn-info w-100" value="Réserver terrain '+ type_terrain.nom +'">');
				var conteneur_boutons_finaux = $('<div class="col-4 ms-4" id="boutons-finaux-'+ type_terrain.id +'" style="display : none;"></div>');
				conteneur_boutons_finaux.append(boutons_finaux);
				row_fin.append(conteneur_boutons_finaux);
				form.append(row_fin);

			});

			types_terrains.forEach((type_terrain) => {
				$('#bouton-'+type_terrain.id).click(function(){
					if ($(this).hasClass("active")) {
						$('#terrains-'+type_terrain.id).show();
						types_terrains.forEach((type_terrainFermer) => {
							if (type_terrain.id != type_terrainFermer.id) {
								$('#bouton-'+type_terrainFermer.id).removeClass("active");
								$('#select-terrains-'+ type_terrainFermer.id).val($('#select-terrains-'+ type_terrainFermer.id +' option:first').val());
								$('#terrains-'+type_terrainFermer.id).hide();
								terrains.forEach((terrainEnlever) => {
									$('#horaires-'+ terrainEnlever.id).hide();
								});
								horaires.forEach((creneauxEnlever) => {
									$("[name=ref_horaires]").removeAttr("checked");
								});
								$('#select-types_reservations-'+ type_terrainFermer.id).val($('#select-types_reservations-'+ type_terrainFermer.id+' option:first').val());
								$('#types_reservations-'+ type_terrainFermer.id).hide();
								$('#boutons-finaux-'+ type_terrainFermer.id).hide();
							}
						});
					}
					else {
						$('#terrains-'+type_terrain.id).hide();
						$('#select-terrains-'+ type_terrain.id).val($('#select-terrains-'+ type_terrain.id+' option:first').val());
						terrains.forEach((terrainEnlever) => {
							$('#horaires-'+ terrainEnlever.id).hide();
						});
						horaires.forEach((creneauxEnlever) => {
							$("[name=ref_horaires]").removeAttr("checked");
						});
						$('#select-types_reservations-'+ type_terrain.id).val($('#select-types_reservations-'+ type_terrain.id+' option:first').val());
						$('#types_reservations-'+ type_terrain.id).hide();
						$('#boutons-finaux-'+ type_terrain.id).hide();
					}
				});
				$('#terrains-'+ type_terrain.id).change(function(){
					horaires.forEach((creneauxEnlever) => {
						$("[name=ref_horaires]").removeAttr("checked");
					});
						terrains.forEach((terrain) => {
							if (terrain.ref_types_terrains == type_terrain.id) {
									if ($('#select-terrains-'+ type_terrain.id+' option:selected').val() == terrain.id) {
										$('#horaires-'+ terrain.id).show();
										horaires.forEach((horaire) => {
											$('#creneaux-'+ terrain.id +'-'+ horaire.id).change(function(){
													$('#types_reservations-'+ type_terrain.id).show();
											});
										});
									}
									else {
										$('#horaires-'+ terrain.id).hide();
									}
							}
					});
				});
				$('#types_reservations-'+ type_terrain.id).change(function(){
					$('#boutons-finaux-'+ type_terrain.id).show();
				});
			});

		}
	}



	const mois = [
	    "Janvier",
	    "Février",
	    "Mars",
	    "Avril",
	    "Mai",
	    "Juin",
	    "Juillet",
	    "Août",
	    "Septembre",
	    "Octobre",
	    "Novembre",
	    "Décembre"
	];



})(jQuery);

</script>

<h2>Réservation de terrain</h2>
<span class="message-message"></span>
<?php if (!isset($_SESSION['auth'])) {?>
	<center><div class="alert alert-danger" role="alert">Il faut être connecté pour réserver !</div></center>
<?php } elseif (isset($_SESSION['auth'])) { ?>
	<?php echo($_SESSION['abonnement'] == 0) ? '<center><div class="alert alert-warning" role="alert"><a href="/abonnement" class="alert-link">Abonnez-vous au club</a> afin de réserver à l\'avance et de profiter de réductions sur vos réservations !</div></center>': '<center><div class="alert alert-success" role="alert">Vous bénéficiez des avantages de l\'abonnement !</div></center>'; ?>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="reservation rounded">
          <div class="calendar-container">
            <div class="calendar">
              <div class="year-header rounded">
            		<span class="year-prev-button" id="prev-year">
									<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                  	<path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
                	</svg>
								</span>
                <span class="year" id="label"></span>
            		<span class="year-next-button" id="next-year">
									<svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                  	<path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                	</svg>
								</span>
              </div>
							<div class="month-header rounded">
            		<span class="month-prev-button" id="prev-month">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16">
                  	<path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"/>
                	</svg>
								</span>
                <span class="month" id="label"></span>
            		<span class="month-next-button" id="next-month">
									<svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16">
                  	<path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"/>
                	</svg>
								</span>
              </div>
              <table class="days-table w-100">
                <td class="day">Lun</td>
                <td class="day">Mar</td>
                <td class="day">Mer</td>
                <td class="day">Jeu</td>
                <td class="day">Ven</td>
                <td class="day">Sam</td>
								<td class="day">Dim</td>
              </table>
              <div class="frame">
                <table class="dates-table w-100">
                  <tbody class="tbody">
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="reservations-container border-start rounded">
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
