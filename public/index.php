<?php
use Router\Router;
use App\Exceptions\NotFoundException;

require'../vendor/autoload.php';

define('VIEWS', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR);
define('SCRIPTS', dirname($_SERVER['SCRIPT_NAME']) . DIRECTORY_SEPARATOR);
define('DB_HOST', 'localhost');
define('DB_NAME', 'padel');
define('DB_USER', 'u_padel');
define('DB_PWD', '2ZjdUi88mhpXtkhS');

$router = new Router($_GET['url']);

$router->get('/','App\Controllers\PagesController@index');

$router->get('/reserver','App\Controllers\PagesController@reserver');
$router->post('/reserver','App\Controllers\PagesController@newReservation');
$router->get('/regles','App\Controllers\PagesController@regles');
$router->get('/boutique','App\Controllers\PagesController@boutique');
$router->get('/faq','App\Controllers\PagesController@faq');

$router->get('/panier','App\Controllers\PagesController@panier');
$router->post('/panier','App\Controllers\PagesController@newCommande');
$router->get('/article/:id','App\Controllers\PagesController@article');
$router->post('/article/:id','App\Controllers\PagesController@ajouterAuPanier');
$router->post('/ajouterPanier/:id','App\Controllers\PagesController@ajouterPanier');
$router->post('/enleverPanier/:id','App\Controllers\PagesController@enleverPanier');
$router->post('/supprimerDuPanier/:id','App\Controllers\PagesController@supprimerDuPanier');

$router->get('/mesreservations','App\Controllers\PagesController@mesreservations');
$router->get('/editReservation/:id','App\Controllers\PagesController@editReservation');
$router->post('/editReservation/:id','App\Controllers\PagesController@updateReservation');
$router->post('/deleteReservation/:id','App\Controllers\PagesController@deleteReservation');

$router->get('/mescommandes','App\Controllers\PagesController@mescommandes');
$router->post('/deleteCommande/:id','App\Controllers\PagesController@deleteCommande');

$router->get('/connexion', 'App\Controllers\UserController@connexion');
$router->post('/connexion', 'App\Controllers\UserController@connexionPost');
$router->get('/logout', 'App\Controllers\UserController@logout');
$router->get('/inscription', 'App\Controllers\UserController@inscription');
$router->post('/inscription', 'App\Controllers\UserController@inscriptionPost');
$router->get('/profil','App\Controllers\PagesController@profil');
$router->post('/profil','App\Controllers\PagesController@profilPost');
$router->get('/motdepasseoublie', 'App\Controllers\UserController@motdepasseoublie');
$router->post('/motdepasseoublie', 'App\Controllers\UserController@motdepasseoubliePost');
$router->get('/changermotdepasse', 'App\Controllers\UserController@changermotdepasse');
$router->post('/changermotdepasse', 'App\Controllers\UserController@changermotdepassePost');
$router->get('/abonnement', 'App\Controllers\UserController@abonnement');
$router->post('/abonnement', 'App\Controllers\UserController@abonnementPost');


$router->get('/admin','App\Controllers\Admin\AdminController@index');
$router->post('/admin','App\Controllers\Admin\AdminController@abonnement');

$router->get('/admin/editTerrain/:id','App\Controllers\Admin\AdminController@editTerrain');
$router->post('/admin/editTerrain/:id','App\Controllers\Admin\AdminController@updateTerrain');
$router->post('/admin/deleteTerrain/:id','App\Controllers\Admin\AdminController@deleteTerrain');
$router->get('/admin/newTerrain','App\Controllers\Admin\AdminController@newTerrain');
$router->post('/admin/newTerrain','App\Controllers\Admin\AdminController@newTerrainPost');

$router->get('/admin/editTypeReservation/:id','App\Controllers\Admin\AdminController@editTypeReservation');
$router->post('/admin/editTypeReservation/:id','App\Controllers\Admin\AdminController@updateTypeReservation');
$router->post('/admin/deleteTypeReservation/:id','App\Controllers\Admin\AdminController@deleteTypeReservation');
$router->get('/admin/newTypeReservation','App\Controllers\Admin\AdminController@newTypeReservation');
$router->post('/admin/newTypeReservation','App\Controllers\Admin\AdminController@newTypeReservationPost');

$router->get('/admin/editReservation/:id','App\Controllers\Admin\AdminController@editReservation');
$router->post('/admin/editReservation/:id','App\Controllers\Admin\AdminController@updateReservation');
$router->post('/admin/deleteReservation/:id','App\Controllers\Admin\AdminController@deleteReservation');

$router->get('/admin/editUtilisateur/:id','App\Controllers\Admin\AdminController@editUtilisateur');
$router->post('/admin/editUtilisateur/:id','App\Controllers\Admin\AdminController@updateUtilisateur');
$router->post('/admin/deleteUtilisateur/:id','App\Controllers\Admin\AdminController@deleteUtilisateur');

$router->get('/admin/editArticle/:id','App\Controllers\Admin\AdminController@editArticle');
$router->post('/admin/editArticle/:id','App\Controllers\Admin\AdminController@updateArticle');
$router->post('/admin/deleteArticle/:id','App\Controllers\Admin\AdminController@deleteArticle');
$router->get('/admin/newArticle','App\Controllers\Admin\AdminController@newArticle');
$router->post('/admin/newArticle','App\Controllers\Admin\AdminController@newArticlePost');

$router->get('/admin/imageArticle/:id','App\Controllers\Admin\AdminController@imageArticle');
$router->post('/admin/imageArticle','App\Controllers\Admin\AdminController@imageArticlePost');
$router->post('/admin/deleteImageArticle/:id','App\Controllers\Admin\AdminController@deleteImageArticle');

$router->get('/admin/editImage/:id','App\Controllers\Admin\AdminController@editImage');
$router->post('/admin/editImage/:id','App\Controllers\Admin\AdminController@updateImage');
$router->post('/admin/deleteImage/:id','App\Controllers\Admin\AdminController@deleteImage');
$router->get('/admin/newImage','App\Controllers\Admin\AdminController@newImage');
$router->post('/admin/newImage','App\Controllers\Admin\AdminController@newImagePost');

$router->get('/admin/editMarque/:id','App\Controllers\Admin\AdminController@editMarque');
$router->post('/admin/editMarque/:id','App\Controllers\Admin\AdminController@updateMarque');
$router->post('/admin/deleteMarque/:id','App\Controllers\Admin\AdminController@deleteMarque');
$router->get('/admin/newMarque','App\Controllers\Admin\AdminController@newMarque');
$router->post('/admin/newMarque','App\Controllers\Admin\AdminController@newMarquePost');

$router->post('/admin/deleteCommande/:id','App\Controllers\Admin\AdminController@deleteCommande');
$router->get('/admin/detailsCommande/:id','App\Controllers\Admin\AdminController@detailsCommande');
$router->post('/admin/detailsCommande/:id','App\Controllers\Admin\AdminController@updateStatut');

try {
  $router->run();
} catch (NotFoundException $e) {
  return $e->error404();
}

 ?>
