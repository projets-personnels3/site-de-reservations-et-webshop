$(document).ready(function(){
    $("#inscriptionForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/inscription",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#inscriptionForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#connexionForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/connexion",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              window.location = data.message;
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#newcommandeForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/panier",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#newcommandeForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ajouteraupanierForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/article/:id",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ajouteraupanierForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#changermotdepasseForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/changermotdepasse",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#changermotdepasseForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#motdepasseoublieForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/motdepasseoublie",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#motdepasseoublieForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#profilForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/profil",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#profilForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#editreservationForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/editReservation/" + id,
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#editreservationForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINeditreservationForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/editReservation/" + id,
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINeditreservationForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINeditutilisateurForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/editUtilisateur/" + id,
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINeditutilisateurForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINeditmarqueForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/editMarque/" + id,
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINeditmarqueForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINnewmarqueForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/newMarque",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINnewmarqueForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINupdatestatutForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/detailsCommande/" + id,
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINupdatestatutForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINnewimageForm").on("submit",function(e){
      e.preventDefault();
      var formData = new FormData($(this)[0]);
      $.ajax({
          url : "/admin/newImage",
          type: "POST",
          dataType: "json",
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINnewimageForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINeditimageForm").on("submit",function(e){
      e.preventDefault();
      var formData = new FormData($(this)[0]);
      $.ajax({
          url : "/admin/editImage/" + id,
          type: "POST",
          dataType: "json",
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINeditimageForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINimagearticleForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/imageArticle",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINimagearticleForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINedittypeForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/editTypeReservation/" + id,
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINedittypeForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINnewtypeForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/newTypeReservation",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINnewtypeForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINabonnementForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin?success=true",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINabonnementForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});

$(document).ready(function(){
    $("#ADMINeditterrainForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/editTerrain/" + id,
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINeditterrainForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
$(document).ready(function(){
    $("#ADMINnewterrainForm").on("submit",function(e){
      e.preventDefault();
      var formData = $(this).serialize();
      $.ajax({
          url : "/admin/newTerrain",
          type: "POST",
          cache:false,
          dataType: "json",
          data: formData,
          success:function(response){
            data = JSON.parse(JSON.stringify(response));
            if (data.error == "0") {
              $("#ADMINnewterrainForm").trigger("reset");
              $('.message-message').replaceWith('<div class="alert alert-success alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
            else if(data.error == "1") {
             $('.message-message').replaceWith('<div class="alert alert-danger alert-dismissible fade show" role="alert"><center>'
               + data.message + '</center><button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div><span class="message-message"></span>');
            }
          }
      });
    });
});
