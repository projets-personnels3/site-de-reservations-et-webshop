<div id="carouselExampleIndicators<?= $articles->Id ?>" class="carousel carousel-dark slide" data-bs-ride="carousel">
  <div class="carousel-inner rounded">
        <?php $compteur_image = 0;
        foreach ($params['images_articles'] as $images_articles) {
              if ($articles->Id == $images_articles->ref_articles) {
                foreach ($params['images'] as $images) {
                  if ($images_articles->ref_images == $images->id) { ?>

                    <div class="carousel-item <?= ($compteur_image === 0) ? "active" : ""; ?>">
                      <a href="/article/<?= $articles->Id ?>">
                        <span class="d-inline-block" title="<?= $articles->Nom ?>">
                          <img src="<?= $images->path ?>" class="card-img-top" alt="Erreur chargement d'image">
                        </span>
                      </a>
                    </div>
            <?php $compteur_image++;
                  }
                }
              }
        }  ?>
  </div>
  <?php if ($compteur_image >= 2) { ?>
    <button class="carousel-control-prev rounded" type="button" data-bs-target="#carouselExampleIndicators<?= $articles->Id ?>" data-bs-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next rounded" type="button" data-bs-target="#carouselExampleIndicators<?= $articles->Id ?>" data-bs-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="visually-hidden">Next</span>
    </button>
  <?php  } ?>
</div>
