<div class="menu-conteneur sticky-top">
  <nav class="navbar navbar-expand-lg navbar bg-success rounded">
    <div class="container-fluid">
      <div class="col-4">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="btn btn-success me-md-2" href="/">Accueil</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-success me-md-2" href="/reserver">Réserver</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-success me-md-2" href="/boutique">Boutique</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-success me-md-2" href="/regles">Règles</a>
          </li>
          <li class="nav-item">
            <a class="btn btn-success me-md-2" href="/faq">FAQ</a>
          </li>
        </ul>
      </div>
      <div class="col-4">
        <h1><a class="liensAccueil" href="/">The Padel Place</a></h1>
      </div>
      <div class="d-flex justify-content-end col-4">
      <?php if(!isset($_SESSION['auth'])) { ?>
          <a href="/connexion" class="btn btn-outline-light me-md-2">Connexion</a>
          <a href="/inscription" class="btn btn-light">Créer un compte</a>
      <?php } else { ?>
        <a class="btn btn-outline-light position-relative me-md-2" href="/panier">Panier
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-cart-fill" viewBox="0 0 16 16">
            <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
          </svg>
        </a>
        <div class="btn-group">
          <button class="btn btn-outline-light me-md-2 dropdown-toggle" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
            <?= $_SESSION['nom']." ".$_SESSION['prenom'] ?>
          </button>
          <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <li><a class="dropdown-item" href="/profil">Mon profil</a></li>
            <li><hr class="dropdown-divider"></li>
            <?php if ($_SESSION['abonnement'] == 0) { ?>
              <li><a class="dropdown-item" href="/abonnement">S'abonner</a></li>
            <?php } elseif ($_SESSION['abonnement'] == 1) { ?>
              <li><a class="dropdown-item" href="/abonnement">Mon abonnement</a></li>
            <?php } ?>
            <li><hr class="dropdown-divider"></li>
            <li><a class="dropdown-item" href="/mesreservations">Mes réservations</a></li>
            <li><a class="dropdown-item" href="/mescommandes">Mes commandes</a></li>
          </ul>
        </div>
      <?php  if ($_SESSION['auth'] == 1) { ?>
        <a href = "/admin?success=true" class="btn btn-info me-md-2">Admin</a>
      <?php } ?>
        <a href="/logout" class="btn btn-danger">Déconnexion</a>
      <?php } ?>
      </div>
    </div>
    </nav>
</div>
