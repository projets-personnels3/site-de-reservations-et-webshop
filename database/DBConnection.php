<?php
namespace Database;
use PDO;
class DBConnection
{
  private $pdo;
  private string $host;
  private string $dbname;
  private string $username;
  private string $password;

  public function __construct(string $host, string $dbname, string $username, string $password)
  {
    $this->host = $host;
    $this->dbname = $dbname;
    $this->username = $username;
    $this->password = $password;
  }

    public function getPDO(): PDO{
      return $this->pdo ?? $this-> pdo = new PDO("mysql:host={$this->host};dbname={$this->dbname}",$this -> username, $this -> password,
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
              PDO::MYSQL_ATTR_INIT_COMMAND => 'SET CHARACTER SET UTF8',
              PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ));
    }

}

 ?>
