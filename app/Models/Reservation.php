<?php
namespace App\Models;

use PDO;

class Reservation extends Model
{
  protected $table = 'reservations';

  public function selectReservations(): array
  {
    $req = $this->db->getPDO()->query('SELECT reservations.id AS Id, utilisateurs.nom AS Nom, utilisateurs.prenom AS Prenom,
    utilisateurs.email AS Email, reservations.dates AS Dates, terrains.nom AS Terrain,
    types_reservations.nom AS Types, horaires.creneaux AS Creneau, reservations.cout AS Cout
FROM reservations
     JOIN utilisateurs
     ON utilisateurs.id = reservations.ref_utilisateurs
     JOIN horaires
     ON horaires.id = reservations.ref_horaires
     JOIN terrains
     ON terrains.id = reservations.ref_terrains
     JOIN types_reservations
     ON types_reservations.id = reservations.ref_types_reservations
WHERE Dates >= NOW() - INTERVAL 1 DAY
ORDER BY Dates, horaires.id');
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    return $req->fetchAll();
  }

  public function selectReservationsById(int $id): array
  {
    $req = $this->db->getPDO()->prepare('SELECT reservations.id AS Id, reservations.dates AS Dates, terrains.nom AS Terrain,
         types_reservations.nom AS Types_reservations, horaires.creneaux AS Creneau, reservations.cout AS Cout,
         types_terrains.nom AS Types_terrains, types_reservations.nombre_personnes AS Nombre_personnes
         FROM reservations
         JOIN horaires
         ON horaires.id = reservations.ref_horaires
         JOIN terrains
         ON terrains.id = reservations.ref_terrains
         JOIN types_reservations
         ON types_reservations.id = reservations.ref_types_reservations
         JOIN types_terrains
         ON types_terrains.id = terrains.ref_types_terrains
    WHERE reservations.ref_utilisateurs = ?
    ORDER BY Dates DESC, horaires.id DESC;');
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }

  public function selectDateReservationsExistantes($dates)
  {
    $req = $this->db->getPDO()->prepare('SELECT * FROM reservations WHERE dates = ? ORDER BY ref_horaires');
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$dates]);
    return $req->fetchAll();
  }

  public function getDerniereReservationByUser(int $id): array
  {
    $req = $this->db->getPDO()->prepare("SELECT MAX(reservations.id) AS id , cout FROM reservations
                                        WHERE reservations.ref_utilisateurs = ?");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }
}
 ?>
