<?php
namespace App\Models;

use PDO;

class Commande_Article extends Model
{
  protected $table = 'commandes_articles';

  public function selectCommandesArticlesByRefCommandes(int $id): array
  {
    $req = $this->db->getPDO()->prepare("SELECT commandes_articles.ref_commandes AS Id, commandes_articles.quantite AS Quantite, articles.nom AS Nom, commandes_articles.tailles AS Tailles, images.path AS Path, articles.nom AS Articles, commandes_articles.cout AS Cout
FROM commandes_articles
JOIN commandes ON commandes.id = commandes_articles.ref_commandes
JOIN articles ON articles.id = commandes_articles.ref_articles
JOIN images_articles ON images_articles.ref_articles = articles.id
JOIN images ON images_articles.ref_images = images.id
WHERE commandes_articles.ref_commandes = ?
GROUP BY commandes_articles.id;");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }
}
 ?>
