<?php
namespace App\Models;

class Utilisateur extends Model
{
  protected $table = 'utilisateurs';

  public function getByEmail(string $email): Utilisateur
  {
    return $this->query("SELECT * FROM {$this->table} WHERE email = ?", [$email], true);
  }

  public function updateMDP(string $nouveauMDP, string $email)
  {
    return $this->query("UPDATE {$this->table} SET mot_de_passe = ? WHERE email = ?", [$nouveauMDP,$email], true);
  }

  public function utilisateurExiste(string $email)
  {
    return $this->query("SELECT COUNT(*) as existe FROM utilisateurs WHERE email = ?", [$email], true);
  }
}
 ?>
