<?php
namespace App\Models;

use PDO;
use Database\DBConnection;

abstract class Model
{
  protected $db;
  protected $table;

  public function __construct(DBConnection $db)
  {
    $this->db = $db;
  }

  public function all(): array
  {
    $req = $this->db->getPDO()->query("SELECT * FROM {$this->table} ORDER BY id");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    return $req->fetchAll();
  }

  public function findById(int $id): Model
  {
    $req = $this->db->getPDO()->prepare("SELECT * FROM {$this->table} WHERE id = ?");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetch();
  }

  public function findByIdReservation(int $id): Model
  {
    $req = $this->db->getPDO()->prepare("SELECT * FROM {$this->table} JOIN reservations ON {$this->table}.id = reservations.ref_{$this->table} WHERE reservations.id = ?");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetch();
  }

  public function findImagesByArticle(int $id): array
  {
    $req = $this->db->getPDO()->prepare("SELECT images.path FROM images JOIN articles
                JOIN images_articles
                ON images_articles.ref_articles = articles.id AND images_articles.ref_images = images.id
                WHERE articles.id = ?");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }

  public function insert(array $data)
  {
    $firstPart = "";
    $secondPart = "";
    $i = 1;

    foreach ($data as $key => $value) {
      $comma = $i == count($data) ? "" : ", ";
      $firstPart .= "{$key}{$comma}";
      $secondPart .= ":{$key}{$comma}";
      $i++;
    }

    return $this->query("INSERT INTO {$this->table} ({$firstPart}) VALUES ($secondPart)", $data, true);
  }

  public function update(int $id, array $data)
  {
    $sqlRequestPart = "";
    $i = 1;
    foreach ($data as $key => $value) {
      $comma = $i == count($data) ? "" : ", ";
      $sqlRequestPart .= "{$key} = :{$key}{$comma}";
      $i++;
    }
    $data['id'] = $id;

    return $this->query("UPDATE {$this->table} SET {$sqlRequestPart} WHERE id = :id", $data, true);
  }

  public function delete(int $id)
  {
    return $this-> query("DELETE FROM {$this->table} WHERE id = ?", [$id], true);
  }

  public function query(string $sql, array $param = null, bool $single = null)
      {
          $method = is_null($param) ? 'query' : 'prepare';

          if (
              strpos($sql, 'DELETE') === 0
              || strpos($sql, 'UPDATE') === 0
              || strpos($sql, 'INSERT INTO') === 0) {

              $stmt = $this->db->getPDO()->$method($sql);
              $stmt->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
              return $stmt->execute($param);
          }

          $fetch = is_null($single) ? 'fetchAll' : 'fetch';

          $stmt = $this->db->getPDO()->$method($sql);
          $stmt->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);

          if ($method === 'query') {
              return $stmt->$fetch();
          } else {
              $stmt->execute($param);
              return $stmt->$fetch();
          }
      }
}

 ?>
