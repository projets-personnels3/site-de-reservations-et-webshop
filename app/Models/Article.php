<?php
namespace App\Models;

use PDO;

class Article extends Model
{
  protected $table = 'articles';

  public function selectArticles(): array
  {
    $req = $this->db->getPDO()->query('SELECT articles.id AS Id, articles.nom AS Nom,
      articles.description AS Description, articles.prix AS Prix, categories.nom AS Categorie,
      categories.id AS RefCategories, marques.nom AS Marque, marques.id AS RefMarques, images.path AS Image
      FROM articles
      JOIN categories
      ON articles.ref_categories = categories.id
      JOIN marques
      ON articles.ref_marques = marques.id
	  LEFT OUTER JOIN images_articles
      ON images_articles.ref_articles = articles.id
      LEFT OUTER JOIN images
      ON images.id = images_articles.ref_images
GROUP BY articles.id
      ORDER BY articles.id;');
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    return $req->fetchAll();
  }

  public function getDernierIdArticle(): array
  {
    $req = $this->db->getPDO()->query("SELECT MAX(articles.id) AS id FROM articles");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    return $req->fetchAll();
  }
}
 ?>
