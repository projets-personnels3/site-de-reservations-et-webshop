<?php
namespace App\Models;

use PDO;

class Image extends Model
{
  protected $table = 'images';

  public function selectImagesDispos(): array
  {
    $req = $this->db->getPDO()->query('SELECT * FROM images WHERE images.associee = 0');
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    return $req->fetchAll();
  }
}
 ?>
