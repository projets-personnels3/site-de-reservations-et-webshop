<?php
namespace App\Models;
use PDO;
class Panier extends Model
{
  protected $table = 'panier';

  public function selectPanierByUtilisateur(int $id): array
  {
    $req = $this->db->getPDO()->prepare('SELECT panier.id, images.path, panier.ref_articles, articles.nom, panier.cout , panier.tailles, panier.quantite FROM articles
                                          JOIN panier ON panier.ref_articles = articles.id
                                          JOIN utilisateurs ON utilisateurs.id = panier.ref_utilisateurs
										                      LEFT OUTER JOIN images_articles
                                          ON images_articles.ref_articles = articles.id
                                          LEFT OUTER JOIN images
                                          ON images.id = images_articles.ref_images
                                          WHERE utilisateurs.id = ?
                                          GROUP BY panier.id');
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }

  public function deleteArticle_PanierParUtilisateur(int $id)
  {
    return $this-> query("DELETE FROM panier WHERE id = ? && ref_utilisateurs = ?", [$id, $_SESSION['IdUtilisateurAuth']], true);
  }

  public function videPanierParUtilisateur()
  {
    return $this-> query("DELETE FROM panier WHERE ref_utilisateurs = ?", [$_SESSION['IdUtilisateurAuth']], false);
  }
}
 ?>
