<?php
namespace App\Models;
use PDO;
class Terrain extends Model
{
  protected $table = 'terrains';

  public function getNomTypeTerrain(int $id): array
  {
    $req = $this->db->getPDO()->prepare("SELECT types_terrains.nom FROM types_terrains
                                          JOIN terrains
                                          ON types_terrains.id = terrains.ref_types_terrains
                                          WHERE terrains.id = ?");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }

  public function selectTerrains(): array
  {
    $req = $this->db->getPDO()->query("SELECT terrains.id AS Id, terrains.nom AS Nom, types_terrains.nom AS Types_terrains
                                        FROM terrains
                                        JOIN types_terrains
                                        ON types_terrains.id = terrains.ref_types_terrains
                                        ORDER BY terrains.id ASC;");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    return $req->fetchAll();
  }
}
 ?>
