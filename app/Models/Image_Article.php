<?php
namespace App\Models;

use PDO;

class Image_Article extends Model
{
  protected $table = 'images_articles';

  public function deleteImageArticleByRefArticle($id)
  {
    $req = $this->db->getPDO()->prepare("DELETE FROM {$this->table} WHERE ref_articles = ?");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }

  public function selectImagesparArticle($id)
  {
    $req = $this->db->getPDO()->prepare("SELECT * FROM images_articles
                                        JOIN images
                                        ON images_articles.ref_images = images.id
                                        WHERE images_articles.ref_articles = ?
                                        ORDER BY `ref_articles`  DESC;");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }

  public function deleteImagesparRefs(int $id, int $ref_images)
  {
    return $this-> query("DELETE FROM images_articles WHERE ref_articles = ? AND ref_images = ?", [$id, $ref_images], true);
  }
}
 ?>
