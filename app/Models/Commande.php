<?php
namespace App\Models;

use PDO;

class Commande extends Model
{
  protected $table = 'commandes';

  public function selectCommandesByUser(int $id): array
  {
    $req = $this->db->getPDO()->prepare("SELECT commandes.id AS Id, statuts.nom AS Statuts, commandes.dates AS Dates FROM commandes
                                        JOIN statuts ON statuts.id = commandes.ref_statuts
                                        WHERE commandes.ref_utilisateurs = ?
                                        ORDER BY commandes.id DESC");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }

  public function selectCommandesById(int $id)
  {
    $req = $this->db->getPDO()->prepare("SELECT commandes.id AS Id, statuts.nom AS Statuts, commandes.dates AS Dates FROM commandes
                                        JOIN statuts ON statuts.id = commandes.ref_statuts
                                        WHERE commandes.id = ?");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetch();
  }

  public function selectCommandes(): array
  {
    $req = $this->db->getPDO()->query("SELECT commandes.id AS Id, statuts.nom AS Statuts, commandes.dates AS Dates, utilisateurs.email AS Clients FROM commandes
                                        JOIN statuts ON statuts.id = commandes.ref_statuts
                                        JOIN utilisateurs ON utilisateurs.id = commandes.ref_utilisateurs");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    return $req->fetchAll();
  }

  public function selectArticlesByCommandesByUser(int $id): array
  {
    $req = $this->db->getPDO()->prepare("SELECT commandes.id AS Id, articles.id AS IdArticles, images.path AS Path, articles.nom AS Articles,
      commandes_articles.cout AS Cout, commandes_articles.tailles AS Tailles, commandes_articles.quantite AS Quantite
      FROM commandes_articles
      JOIN commandes ON commandes.id = commandes_articles.ref_commandes
      JOIN articles ON articles.id = commandes_articles.ref_articles
      LEFT OUTER JOIN images_articles
      ON images_articles.ref_articles = articles.id
      LEFT OUTER JOIN images
      ON images.id = images_articles.ref_images
      WHERE commandes.ref_utilisateurs = ?
      GROUP BY commandes_articles.id;");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }

  public function getDerniereCommandeByUser(int $id): array
  {
    $req = $this->db->getPDO()->prepare("SELECT MAX(commandes.id) AS id FROM commandes
                                        WHERE commandes.ref_utilisateurs = ?");
    $req->setFetchMode(PDO::FETCH_CLASS, get_class($this), [$this->db]);
    $req->execute([$id]);
    return $req->fetchAll();
  }
}
 ?>
