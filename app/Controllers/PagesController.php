<?php

namespace App\Controllers;

use App\Models\Horaire;
use App\Models\Reservation;
use App\Models\Terrain;
use App\Models\Utilisateur;
use App\Models\Article;
use App\Models\Categorie;
use App\Models\Marque;
use App\Models\Image;
use App\Models\Image_Article;
use App\Models\Commande_Article;
use App\Models\Commande;
use App\Models\Statut;
use App\Models\Panier;
use App\Models\Type_Reservation;
use App\Models\Type_Terrain;
use App\Models\Abonnement;

class PagesController extends Controller{

  public function index()
  {
    return $this->view('pages/index');
  }

  public function reserver()
  {
    $terrains = new Terrain($this->getDB());
    $terrains = $terrains->all();

    $horaires = new Horaire($this->getDB());
    $horaires = $horaires->all();

    $reservations = new Reservation($this->getDB());
    $reservations = $reservations->all();

    $types_reservations = new Type_Reservation($this->getDB());
    $types_reservations = $types_reservations->all();

    $types_terrains = new Type_Terrain($this->getDB());
    $types_terrains = $types_terrains->all();

    return $this->view('pages/reserver', compact('terrains','horaires','reservations','types_reservations','types_terrains'));
  }

  public function newReservation()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $type_reservation = (new Type_Reservation($this->getDB()))->findById($_POST['ref_types_reservations']);
    $_SESSION['abonnement'] == 0 ? $cout = $type_reservation->prix : $cout = $type_reservation->prix_abonne;

    $json= json_encode($_POST);
    $array = json_decode($json, true);
    $array['cout'] = $cout;

    $reservation = new Reservation($this->getDB());
    $result = $reservation->insert($array);
    if ($result) {
      $terrain = (new Terrain($this->getDB()))->findById($_POST['ref_terrains']);
      $horaire = (new Horaire($this->getDB()))->findById($_POST['ref_horaires']);
      $types_reservations = (new Type_Reservation($this->getDB()))->findById($_POST['ref_types_reservations']);
      $nom_type_terrain = (new Terrain($this->getDB()))->getNomTypeTerrain($_POST['ref_terrains']);
      $derniere_reservation = (new Reservation($this->getDB()))->getDerniereReservationByUser($_SESSION['IdUtilisateurAuth']);

      $date = date_create($_POST['dates']);

      $dates = date_format($date,"d/m/Y");
      $to = $_SESSION['email'];
      $subject = 'Réservation du '.$dates.' confirmée.';
      $msg = "<html>
                <body>
                  <h3>Merci pour votre réservation ".$_SESSION['prenom']." !</h3>
                 <p><b>N° de réservation : </b>".$derniere_reservation[0]->id."</p>
                 <p><b>Date : </b>".$dates."</p>
                 <p><b>Type de terrain : </b>".$nom_type_terrain[0]->nom."</p>
                 <p><b>Terrain : </b>".$terrain->nom."</p>
                 <p><b>Créneau : </b>".$horaire->creneaux."</p>
                 <p><b>Type de terrain : </b>".$types_reservations->nom."</p>
                 <p><b>Nombre de personne(s) : </b>".$types_reservations->nombre_personnes."</p>
                 <p><b>Prix : </b>".$derniere_reservation[0]->cout."€ à payer sur place</p>
               </body>
             </html>";
     $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
     $headers .= "MIME-version: 1.0\r\n";
     $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
     mail($to, $subject, $msg, $headers);
     return header('Location: /mesreservations');
    }
  }

  public function editReservation(int $id)
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $reservation = (new Reservation($this->getDB()))->findById($id);
    $terrain = (new Terrain($this->getDB()))->findByIdReservation($id);
    $horaire = (new Horaire($this->getDB()))->findByIdReservation($id);
    $utilisateur = (new Utilisateur($this->getDB()))->findByIdReservation($id);
    $terrains = (new Terrain($this->getDB()))->all();
    $horaires = (new Horaire($this->getDB()))->all();
    return $this->view('pages/editReservation', compact('reservation','terrain', 'horaire', 'utilisateur', 'terrains', 'horaires'));
  }

  public function updateReservation(int $id)
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $reservation = new Reservation($this->getDB());
    $testReservationExistante = (new Reservation($this->getDB()))->all();

    $boolReservationExistante = 0;

    foreach ($testReservationExistante as $testReservation)
    {
      if ($testReservation->dates == $_POST['dates'] && $testReservation->ref_terrains == $_POST['ref_terrains'] && $testReservation->ref_horaires == $_POST['ref_horaires'])
      {
        $boolReservationExistante = 1;
        break;
      }
    }

    if ($boolReservationExistante == 0) {
      $present = date('Y-m-d');
      if ($present < $_POST['dates']) {
        $reservation->update($id, $_POST);
        echo json_encode(array('error'=>'0', 'message'=>'Votre réservation a bien été modifiée ! Vous allez recevoir un email de confirmation.'));

        $terrain = (new Terrain($this->getDB()))->findById($_POST['ref_terrains']);
        $horaire = (new Horaire($this->getDB()))->findById($_POST['ref_horaires']);
        $date = date_create($_POST['dates']);
        $dates = date_format($date,"d/m/Y");
        $to = $_SESSION['email'];
        $subject = 'Modification de votre réservation confirmée.';
        $msg = "<html>
                  <body>
                    <h3>Merci pour votre réservation ".$_SESSION['prenom']." !</h3>
                    <p><b>Date : </b>".$dates."</p>
                    <p><b>Terrain : </b>".$terrain->nom."</p>
                    <p><b>Créneau : </b>".$horaire->creneaux."</p>
                    <p><b>Prix : </b>36.00€ à payer sur place</p>
                  </body>
                </html>";
        $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
        $headers .= "MIME-version: 1.0\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
        mail($to, $subject, $msg, $headers);
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Veuillez choisir une date située dans le futur !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Ce créneau horaire est déjà réservé !'));
    }
  }

  public function deleteReservation(int $id)
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $reservations = new Reservation($this->getDB());
    $reservation = (new Reservation($this->getDB()))->findById($id);
    $datereservation = $reservation->dates;
    $result = $reservations->delete($id);

    if ($result) {
      $date = date_create($datereservation);

      $dates = date_format($date,"d/m/Y");
      $to = $_SESSION['email'];
      $subject = 'Réservation du '.$dates.' annulée.';
      $msg = "Annulation de votre réservation du ".$dates.".";
      $msg = "<html>
                <body>
                  <h3>Vous avez annuler votre réservation du ".$dates." !</h3>
                </body>
              </html>";
      $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
      $headers .= "MIME-version: 1.0\r\n";
      $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
      mail($to, $subject, $msg, $headers);
      return header('Location: /mesreservations');
    }
  }

  public function mesreservations()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $reservations = (new Reservation($this->getDB()))->selectReservationsById($_SESSION['IdUtilisateurAuth']);
    return $this->view('pages/mesreservations', compact('reservations'));
  }

  public function regles()
  {
    return $this->view('pages/regles');
  }

  public function boutique()
  {
    $articles = new Article($this->getDB());
    $articles = $articles->selectArticles();

    $categories = new Categorie($this->getDB());
    $categories = $categories->all();

    $marques = new Marque($this->getDB());
    $marques = $marques->all();

    return $this->view('pages/boutique', compact('articles','categories','marques'));
  }

  public function faq(){
    return $this->view('pages/faq');
  }

  public function panier()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $panier = (new Panier($this->getDB()))->selectPanierByUtilisateur($_SESSION['IdUtilisateurAuth']);
    return $this->view('pages/panier', compact('panier'));
  }

  public function ajouterAuPanier()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $panier = new Panier($this->getDB());
    $testarticlepanier = (new Panier($this->getDB()))->selectPanierByUtilisateur($_SESSION['IdUtilisateurAuth']);
    $booltest = 0;
    foreach ($testarticlepanier as $test) {
      if ($test->ref_articles == $_POST['ref_articles'] && $test->tailles == $_POST['tailles']) {
        $quantite = $test->quantite;
        $quantite++;
        $array = array ("quantite" => $quantite);
        $result = $panier->update($test->id, $array);
        $booltest = 1;
        break;
      }
    }
    if ($booltest == 0) {
      $result = $panier->insert($_POST);
    }

    if ($result) {
      echo json_encode(array('error'=>'0', 'message'=>'L\'article a bien été ajouté à votre panier !'));
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Erreur d\'ajout d\'article au panier !'));
    }
  }

  public function ajouterPanier(int $id)
  {
    $articlePanier = (new Panier($this->getDB()))->findById($id);
    $quantite = $articlePanier->quantite;
    $quantite++;
    $array = array ("quantite" => $quantite);
    $result = $articlePanier->update($id, $array);
    return header('Location: /panier');
  }

  public function enleverPanier(int $id)
  {
    $articlePanier = (new Panier($this->getDB()))->findById($id);
    $quantite = $articlePanier->quantite;
    $quantite--;
    $array = array ("quantite" => $quantite);

    if ($quantite == 0) {
      $articlePanier->deleteArticle_PanierParUtilisateur($id);
    }
    else {
      $articlePanier->update($id, $array);
    }
    return header('Location: /panier');
  }

  public function supprimerDuPanier(int $id)
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $panier = new Panier($this->getDB());
    $result = $panier->deleteArticle_PanierParUtilisateur($id);

    if ($result) {
      return header('Location: /panier');
    }
  }

  public function newCommande()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $commande = new Commande($this->getDB());
    $commande_article = new Commande_Article($this->getDB());
    $panier = (new Panier($this->getDB()))->selectPanierByUtilisateur($_SESSION['IdUtilisateurAuth']);
    $utilisateur = (new Utilisateur($this->getDB()))->findById($_SESSION['IdUtilisateurAuth']);


      $commande->insert($_POST);

      $getdernierecommande = (new Commande($this->getDB()))->getDerniereCommandeByUser($_SESSION['IdUtilisateurAuth']);

      foreach ($panier as $articles_panier) {
        $array = array (
      "ref_commandes" => $getdernierecommande[0]->id,
      "ref_articles" => $articles_panier->ref_articles,
      "tailles"   => $articles_panier->tailles,
      "quantite"   => $articles_panier->quantite,
      "cout" => $articles_panier->cout);

        $commande_article->insert($array);
      }
        $viderPanier = (new Panier($this->getDB()))->videPanierParUtilisateur();
        $date = date_create($_POST['dates']);
        $dates = date_format($date,"d/m/Y");
        $to = $_SESSION['email'];
        $subject = 'Votre commande du '.$dates.' est confirmée.';
        $msg = "<html>
                  <body>
                    <h3>Merci pour votre commande n°".$getdernierecommande[0]->id.", ".$_SESSION['prenom']." !</h3>
                    <p>Nous la traiterons dans les plus brefs délais.</p>
                    <p>Vous pouvez consulter les détails de votre commande à tout moment dans votre profil sur le site.</p>
                    <p>Elle vous sera envoyée à l'adresse que vous avez renseignée dans votre profil :</p>
                    <p><b>Adresse : </b>".$utilisateur->adresse."</p>
                    <p><b>Ville : </b>".$utilisateur->ville."</p>
                    <p><b>Code postal : </b>".$utilisateur->code_postal."</p>
                  </body>
                </html>";
        $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
        $headers .= "MIME-version: 1.0\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
        mail($to, $subject, $msg, $headers);
        return header('Location: /mescommandes');

  }

  public function article(int $id)
  {
    $article = (new Article($this->getDB()))->findById($id);
    $marques = (new Marque($this->getDB()))->all();
    $images = (new Image($this->getDB()))->findImagesByArticle($id);
    return $this->view('pages/article', compact('article','marques','images'));
  }

  public function profil()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $utilisateur = (new Utilisateur($this->getDB()))->findById($_SESSION['IdUtilisateurAuth']);
    return $this->view('pages/profil', compact('utilisateur'));
  }

  public function profilPost()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $utilisateur = (new Utilisateur($this->getDB()))->findById($_SESSION['IdUtilisateurAuth']);

    $_POST['nom'] = htmlspecialchars(strip_tags(ucwords(mb_strtolower($_POST['nom']))));
    $_POST['prenom'] = htmlspecialchars(strip_tags(ucwords(mb_strtolower($_POST['prenom']))));
    $_POST['adresse'] = htmlspecialchars(strip_tags($_POST['adresse']));
    $_POST['ville'] = htmlspecialchars(strip_tags(ucwords(mb_strtolower($_POST['ville']))));

    if (password_verify($_POST['mot_de_passe'], $utilisateur->mot_de_passe)) {
      $_POST['mot_de_passe'] = password_hash($_POST['mot_de_passe'], PASSWORD_DEFAULT);
      if ($_POST['nom'] != $utilisateur->nom || $_POST['prenom'] != $utilisateur->prenom || $_POST['adresse'] != $utilisateur->adresse || $_POST['code_postal'] != $utilisateur->code_postal) {
        $utilisateur-> update($_SESSION['IdUtilisateurAuth'], $_POST);
        echo json_encode(array('error'=>'0', 'message'=>'Votre profil a bien été modifié !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Aucun changement effectué'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Le mot de passe n\'est pas correct'));
    }
  }

  public function mescommandes()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $commandes = (new Commande($this->getDB()))->selectCommandesByUser($_SESSION['IdUtilisateurAuth']);
    $commandes_articles = (new Commande($this->getDB()))->selectArticlesByCommandesByUser($_SESSION['IdUtilisateurAuth']);
    return $this->view('pages/mescommandes', compact('commandes','commandes_articles'));
  }

  public function deleteCommande(int $id)
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $infocommande = (new Commande($this->getDB()))->findById($id);

    $date = date_create($infocommande->dates);
    $dates = date_format($date,"d/m/Y");
    $to = $_SESSION['email'];
    $subject = 'Votre commande du '.$dates.' est annulée.';
    $msg = "<html>
              <body>
                <h3>Vous avez annulé votre commande qui avait le n° ".$infocommande->id."</h3>
              </body>
            </html>";
    $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
    $headers .= "MIME-version: 1.0\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
    mail($to, $subject, $msg, $headers);

    $commande = (new Commande($this->getDB()))->findById($id);
    $result = $commande->delete($id);

    if ($result) {
      return header('Location: /mescommandes');
    }
  }
}

 ?>
