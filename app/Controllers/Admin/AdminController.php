<?php

namespace App\Controllers\Admin;

use App\Controllers\Controller;
use App\Models\Reservation;
use App\Models\Utilisateur;
use App\Models\Horaire;
use App\Models\Terrain;
use App\Models\Article;
use App\Models\Categorie;
use App\Models\Marque;
use App\Models\Image;
use App\Models\Image_Article;
use App\Models\Commande_Article;
use App\Models\Commande;
use App\Models\Statut;
use App\Models\Type_Reservation;
use App\Models\Type_Terrain;
use App\Models\Abonnement;

class AdminController extends Controller
{
  public function index()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $reservations = (new Reservation($this->getDB()))->selectReservations();
    $utilisateurs = (new Utilisateur($this->getDB()))->all();
    $articles = (new Article($this->getDB()))->selectArticles();
    $marques = (new Marque($this->getDB()))->all();
    $images = (new Image($this->getDB()))->all();
    $images_articles = (new Image_Article($this->getDB()))->all();
    $commandes = (new Commande($this->getDB()))->selectCommandes();
    $types_reservations = (new Type_Reservation($this->getDB()))->all();
    $types_terrains = (new Type_Terrain($this->getDB()))->all();
    $terrains = (new Terrain($this->getDB()))->selectTerrains();
    $abonnement = (new Abonnement($this->getDB()))->all();

    return $this->view('admin/index', compact('reservations','utilisateurs','articles','marques','images','images_articles','commandes','types_reservations','types_terrains','terrains','abonnement'));
  }

  public function abonnement()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $abonnement = new Abonnement($this->getDB());
    $verifChangement = (new Abonnement($this->getDB()))->findById(1);

    if ($verifChangement->prix == $_POST['prix']) {
      echo json_encode(array('error'=>'1', 'message'=>'Le prix de l\'abonnement est inchangé !'));
    }
    else {
      $abonnement->update(1,$_POST);
      echo json_encode(array('error'=>'0', 'message'=>'Modification du prix d\'un abonnement réussi !'));
    }
  }

  public function editTerrain(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $terrain = (new Terrain($this->getDB()))->findById($id);
    $types_terrains = (new Type_Terrain($this->getDB()))->all();

    return $this->view('admin/editTerrain', compact('terrain','types_terrains'));
  }

  public function updateTerrain(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $terrain = new Terrain($this->getDB());
    $testTerrainExistante = (new Terrain($this->getDB()))->all();

    $boolTerrainExistante = 0;
    foreach ($testTerrainExistante as $testTerrain)
    {
      if ($testTerrain->nom == $_POST['nom'] && $testTerrain->ref_types_terrains == $_POST['ref_types_terrains'])
      {
        $boolTerrainExistante = 1;
      }
    }

    if ($boolTerrainExistante == 0) {
      $result = $terrain-> update($id, $_POST);
      if ($result) {
        echo json_encode(array('error'=>'0', 'message'=>'Modification du terrain réussie !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de la modification du terrain !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Ce terrain est déjà dans la base de données !'));
    }
  }

  public function deleteTerrain(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $terrain = new Terrain($this->getDB());
    $result = $terrain->delete($id);

    if ($result) {
      return header('Location: /admin?success=true');
    }
  }

  public function newTerrain()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $types_terrains = (new Type_Terrain($this->getDB()))->all();

    return $this->view('admin/newTerrain', compact('types_terrains'));
  }

  public function newTerrainPost()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $terrain = new Terrain($this->getDB());
    $testTerrainExistante = (new Terrain($this->getDB()))->all();

    $boolTerrainExistante = 0;
    foreach ($testTerrainExistante as $testTerrain)
    {
      if ($testTerrain->nom == $_POST['nom'])
      {
        $boolTerrainExistante = 1;
      }
    }

    if ($boolTerrainExistante == 0) {
    $result = $terrain->insert($_POST);
      if ($result) {
        echo json_encode(array('error'=>'0', 'message'=>'Ajout du terrain réussi !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de l\'ajout du terrain !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Ce terrain est déjà dans la base de données !'));
    }
  }

  public function editTypeReservation(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $types = (new Type_Reservation($this->getDB()))->findById($id);

    return $this->view('admin/editTypeReservation', compact('types'));
  }

  public function updateTypeReservation(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $types = new Type_Reservation($this->getDB());
    $testTypeExistant = (new Type_Reservation($this->getDB()))->all();

    $boolTypeExistant = 0;
    foreach ($testTypeExistant as $testType)
    {
      if ($testType->nom == $_POST['nom'] && $testType->nombre_personnes == $_POST['nombre_personnes'] && $testType->prix == $_POST['prix'] && $testType->prix_abonne == $_POST['prix_abonne'])
      {
        $boolTypeExistant = 1;
      }
    }

    if ($boolTypeExistant == 0) {
      if ($_POST['prix_abonne'] < $_POST['prix']) {
        $types->update($id, $_POST);
        echo json_encode(array('error'=>'0', 'message'=>'Modification du type de réservations réussi !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Le prix normal doit être plus grand que le prix abonné !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Ce type de réservations est déjà dans la base de données !'));
    }
  }

  public function deleteTypeReservation(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $types = new Type_Reservation($this->getDB());
    $result = $types->delete($id);

    if ($result) {
      return header('Location: /admin?success=true');
    }
  }

  public function newTypeReservation()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    return $this->view('admin/newTypeReservation');
  }

  public function newTypeReservationPost()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $types = new Type_Reservation($this->getDB());
    $testTypeExistant = (new Type_Reservation($this->getDB()))->all();

    $boolTypeExistant = 0;
    foreach ($testTypeExistant as $testType)
    {
      if ($testType->nom == $_POST['nom'])
      {
        $boolTypeExistant = 1;
      }
    }

    if ($boolTypeExistant == 0) {
      if ($_POST['prix_abonne'] < $_POST['prix']) {
        $types->insert($_POST);
        echo json_encode(array('error'=>'0', 'message'=>'Ajout du type de réservations réussi !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Le prix normal doit être plus grand que le prix abonné !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Ce type de réservations est déjà dans la base de données !'));
    }
  }

  public function editReservation(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $reservation = (new Reservation($this->getDB()))->findById($id);
    $terrain = (new Terrain($this->getDB()))->findByIdReservation($id);
    $horaire = (new Horaire($this->getDB()))->findByIdReservation($id);
    $utilisateur = (new Utilisateur($this->getDB()))->findByIdReservation($id);
    $terrains = (new Terrain($this->getDB()))->all();
    $horaires = (new Horaire($this->getDB()))->all();

    return $this->view('admin/editReservation', compact('reservation','terrain', 'horaire', 'utilisateur', 'terrains', 'horaires'));
  }

  public function updateReservation(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $reservation = new Reservation($this->getDB());
    $testReservationExistante = (new Reservation($this->getDB()))->all();

    $boolReservationExistante = 0;

    foreach ($testReservationExistante as $testReservation)
    {
      if ($testReservation->dates == $_POST['dates'] && $testReservation->ref_terrains == $_POST['ref_terrains'] && $testReservation->ref_horaires == $_POST['ref_horaires'])
      {
        $boolReservationExistante = 1;
      }
    }

    if ($boolReservationExistante == 0) {
      $present = date('Y-m-d');
      if ($present < $_POST['dates']) {
        $reservation->update($id, $_POST);
        echo json_encode(array('error'=>'0', 'message'=>'Cette réservation a bien été modifiée ! Un email vient d\'être envoyé à l\'utilisateur concerné.'));

        $terrain = (new Terrain($this->getDB()))->findById($_POST['ref_terrains']);
        $horaire = (new Horaire($this->getDB()))->findById($_POST['ref_horaires']);
        $utilisateur = (new Utilisateur($this->getDB()))->findById($_POST['ref_utilisateurs']);

        $date = date_create($_POST['dates']);
        $dates = date_format($date,"d/m/Y");
        $to = $utilisateur->email;
        $subject = 'Modification de votre réservation par un administrateur.';
        $msg = "<html>
                  <body>
                    <h3>Un administrateur a du modifier votre réservation.</h3>
                    <p>Bonjour ".$utilisateur->prenom."</p>
                    <p>Dorénavant, votre réservation est comme ceci :</p>
                    <p><b>Date : </b>".$dates."</p>
                    <p><b>Terrain : </b>".$terrain->nom."</p>
                    <p><b>Créneau : </b>".$horaire->creneaux."</p>
                    <p><b>Prix : </b>36.00€ à payer sur place</p>
                  </body>
                </html>";
        $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
        $headers .= "MIME-version: 1.0\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
        mail($to, $subject, $msg, $headers);
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Veuillez choisir une date située dans le futur !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Ce créneau horaire est déjà réservé !'));
    }
  }

  public function deleteReservation(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $inforeservation = (new Reservation($this->getDB()))->findById($id);
    $terrain = (new Terrain($this->getDB()))->findById($inforeservation->ref_terrains);
    $horaire = (new Horaire($this->getDB()))->findById($inforeservation->ref_horaires);
    $utilisateur = (new Utilisateur($this->getDB()))->findById($inforeservation->ref_utilisateurs);

    $date = date_create($inforeservation->dates);
    $dates = date_format($date,"d/m/Y");
    $to = $utilisateur->email;
    $subject = 'Annulation de votre réservation par un administrateur.';
    $msg = "<html>
              <body>
                <h3>Un administrateur a du annuler votre réservation.</h3>
                <p>Bonjour ".$utilisateur->prenom."</p>
                <p>Nous sommes contraint d'annuler votre réservation du <b>".$dates."</b></p>
                <p><b>Terrain : </b>".$terrain->nom."</p>
                <p><b>Créneau : </b>".$horaire->creneaux."</p>
                <p>Veuillez nous excusez pour ce désagrément.</p>
                <p>En espérant vous revoir bientôt chez nous !</p>
              </body>
            </html>";
    $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
    $headers .= "MIME-version: 1.0\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
    mail($to, $subject, $msg, $headers);

    $reservation = new Reservation($this->getDB());
    $result = $reservation->delete($id);

    if ($result) {
      return header('Location: /admin?success=true');
    }
  }

  public function editUtilisateur(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $utilisateur = (new Utilisateur($this->getDB()))->findById($id);
    return $this->view('admin/editUtilisateur', compact('utilisateur'));
  }

  public function updateUtilisateur(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $infoutilisateur = (new Utilisateur($this->getDB()))->findById($id);

    $changementAdmin = 0;
    if ($infoutilisateur->admin != $_POST['admin']) {
      $changementAdmin = 1;
    }

    $changementAbonnement = 0;
    if ($infoutilisateur->abonnement != $_POST['abonnement']) {
      $changementAbonnement = 1;
    }

    $utilisateur = new Utilisateur($this->getDB());
    $utilisateur-> update($id, $_POST);

    if ($_POST['admin'] == 1 && $changementAdmin == 1) {
      echo json_encode(array('error'=>'0', 'message'=>'Utilisateur promu au rang d\'administrateur'));

      $to = $infoutilisateur->email;
      $subject = 'Félicitations, vous êtes désormais administrateur !';
      $msg = "<html>
                <body>
                  <h3>Un administrateur vous a promu au rang d'administrateur.</h3>
                  <p>".$infoutilisateur->prenom.", bienvenue dans la famille The Padel Place !</p>
                </body>
              </html>";
      $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
      $headers .= "MIME-version: 1.0\r\n";
      $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
      mail($to, $subject, $msg, $headers);
    }
    elseif ($_POST['admin'] != 1 && $changementAdmin == 1) {
      echo json_encode(array('error'=>'0', 'message'=>'Utilisateur rétrogradé comme simple utilisateur'));

      $to = $infoutilisateur->email;
      $subject = 'Un administrateur vous a rétrogradé comme simple utilisateur.';
      $msg = "<html>
                <body>
                  <h3>Un administrateur vous a rétrogradé comme simple utilisateur.</h3>
                </body>
              </html>";
      $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
      $headers .= "MIME-version: 1.0\r\n";
      $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
      mail($to, $subject, $msg, $headers);
    }
    elseif ($changementAdmin == 0) {
      //ne fais rien
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de la modification !'));
    }

    if ($_POST['abonnement'] == 1 && $changementAbonnement == 1) {
      echo json_encode(array('error'=>'0', 'message'=>'Utilisateur est désormais abonné !'));

      $to = $infoutilisateur->email;
      $subject = 'Félicitations, vous êtes désormais abonné !';
      $msg = "<html>
                <body>
                  <h3>".$infoutilisateur->nom." ".$infoutilisateur->prenom.", bienvenue dans la famille The Padel Place !</h3>
                </body>
              </html>";
      $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
      $headers .= "MIME-version: 1.0\r\n";
      $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
      mail($to, $subject, $msg, $headers);
    }
    elseif ($_POST['abonnement'] != 1 && $changementAbonnement == 1) {
      echo json_encode(array('error'=>'0', 'message'=>'Utilisateur n\'est plus abonné !'));

      $to = $infoutilisateur->email;
      $subject = 'Un administrateur a mis fin à votre abonnement.';
      $msg = "<html>
                <body>
                  <h3>Un administrateur a mis fin à votre abonnement.</h3>
                </body>
              </html>";
      $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
      $headers .= "MIME-version: 1.0\r\n";
      $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
      mail($to, $subject, $msg, $headers);
    }
    elseif ($changementAbonnement == 0) {
      //ne fais rien
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de la modification !'));
    }
    if ($changementAdmin == 0 && $changementAbonnement == 0) {
      echo json_encode(array('error'=>'1', 'message'=>'Aucune modification effectuée'));
    }
  }

  public function editArticle(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $article = (new Article($this->getDB()))->findById($id);
    $categories = (new Categorie($this->getDB()))->all();
    $marques = (new Marque($this->getDB()))->all();
    $images_articles = (new Image_Article($this->getDB()))->selectImagesparArticle($id);
    $imagesdisponibles = (new Image($this->getDB()))->selectImagesDispos();

    return $this->view('admin/editArticle', compact('article','categories','marques','images_articles','imagesdisponibles'));
  }

  public function updateArticle(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    if (isset($_POST['ref_images'],$_POST['associer'])) {
      $arrayimages = array();
      foreach ($_POST['ref_images'] as $ref_images) {
        array_push($arrayimages,$ref_images);
      }
        $image_article = new Image_Article($this->getDB());
        foreach ($arrayimages as $key => $value) {
          $array = array('ref_images' => $value, 'ref_articles' => $id);
          $image_article->insert($array);
          $associee = array ("associee" => 1);
          $image = new Image($this->getDB());
          $image->update($value, $associee);
      }
      $images = (new Image($this->getDB()))->selectImagesDispos();
      $images_articles = (new Image_Article($this->getDB()))->selectImagesparArticle($id);
      echo json_encode(array('error'=>'0', 'message'=>'Image(s) associée(s) à l\'article !', 'images' => $images, 'images_articles' => $images_articles));
    }
    elseif (isset($_FILES['path']['name'])) {
      $image = new Image($this->getDB());
      $testImageExistante = (new Image($this->getDB()))->all();

      $rootImage = "../../ressources/images/articles/";
      $cheminImage = $rootImage . $_FILES['path']['name'];

      $array = array ("path" => $cheminImage);

      $boolImageExistante = 0;
      foreach ($testImageExistante as $testImage)
      {
        if ($testImage->path == $cheminImage)
        {
          $boolImageExistante = 1;
          break;
        }
      }

      if ($boolImageExistante == 0) {
      $result = $image->insert($array);
        if ($result) {
          $images = (new Image($this->getDB()))->selectImagesDispos();
          echo json_encode(array('error'=>'0', 'message'=>'Ajout de l\'image réussi !', 'images' => $images));
        }
        else {
          echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de l\'ajout de l\'image !'));
        }
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Cette image est déjà dans la base de données !'));
      }
    }
    if (isset($_POST['ref_images'],$_POST['dissocier'])) {
      $arrayimages = array();
      foreach ($_POST['ref_images'] as $ref_images) {
        array_push($arrayimages,$ref_images);
        $image_article = (new Image_Article($this->getDB()))->deleteImagesparRefs($id,$ref_images);
      }
        foreach ($arrayimages as $key => $value) {
          $dissociee = array ("associee" => 0);
          $image = new Image($this->getDB());
          $image->update($value, $dissociee);
      }
      $images = (new Image($this->getDB()))->selectImagesDispos();
      $images_articles = (new Image_Article($this->getDB()))->selectImagesparArticle($id);
      echo json_encode(array('error'=>'0', 'message'=>'Image(s) dissociée(s) de l\'article !', 'images' => $images, 'images_articles' => $images_articles));
    }
    elseif (isset($_POST['nom']) && !isset($_POST['description'],$_POST['ref_marques'],$_POST['prix'],$_POST['ref_categories'])) {
      $marque = new Marque($this->getDB());
      $testMarqueExistante = (new Marque($this->getDB()))->all();

      $boolMarqueExistante = 0;
      foreach ($testMarqueExistante as $testMarque)
      {
        if ($testMarque->nom == $_POST['nom'])
        {
          $boolMarqueExistante = 1;
        }
      }

      if ($boolMarqueExistante == 0) {
      $result = $marque->insert($_POST);
        if ($result) {
          $marques = (new Marque($this->getDB()))->all();
          echo json_encode(array('error'=>'0', 'message'=>'Ajout de la marque réussi !', 'marques'=> $marques));
        }
        else {
          echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de l\'ajout de la marque !'));
        }
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Cette marque est déjà dans la base de données !'));
      }
    }
    elseif (isset($_POST['nom'],$_POST['description'],$_POST['ref_marques'],$_POST['prix'],$_POST['ref_categories'])) {
      $article = new Article($this->getDB());
      $testArticleExistant = (new Article($this->getDB()))->all();

      $boolArticleExistant = 0;
      foreach ($testArticleExistant as $testArticle)
      {
        if ($testArticle->nom == $_POST['nom'] && $testArticle->description == $_POST['description'] && $testArticle->prix == $_POST['prix'] && $testArticle->ref_categories == $_POST['ref_categories'] && $testArticle->ref_marques == $_POST['ref_marques'])
        {
          $boolArticleExistant = 1;
        }
      }

      if ($boolArticleExistant == 0) {
        $result = $article-> update($id, $_POST);
        if ($result) {
          echo json_encode(array('error'=>'0', 'message'=>'Modification de l\'article réussie !'));
        }
        else {
          echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de la modification de l\'article !'));
        }
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Cet article est déjà dans la base de données !'));
      }
    }
  }

  public function deleteArticle(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $commandes_articles = (new Commande_Article($this->getDB()))->all();
    $boolArticleDansUneCommande = 0;

    foreach ($commandes_articles as $commande_article) {
      if ($commande_article->ref_articles == $id) {
        $boolArticleDansUneCommande = 1;
        break;
      }
    }

      if ($boolArticleDansUneCommande == 0) {
        $image_Article = (new Image_Article($this->getDB()))->all();
        $image = new Image($this->getDB());

        $array = array ("associee" => 0);

        foreach ($image_Article as $imageArticle) {
          if ($id == $imageArticle->ref_articles) {
            $image->update($imageArticle->ref_images, $array);
            $result = $imageArticle->delete($imageArticle->id);
          }
        }

        $article = new Article($this->getDB());
        $result = $article->delete($id);

          if ($result) {
            return header('Location: /admin?success=true');
          }
      }
      else {
        return header('Location: /admin?success=true');
      }
  }

  public function newArticle()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $categories = (new Categorie($this->getDB()))->all();
    $marques = (new Marque($this->getDB()))->all();
    $imagesdisponibles = (new Image($this->getDB()))->selectImagesDispos();

    return $this->view('admin/newArticle',compact('categories','marques','imagesdisponibles'));
  }

  public function newArticlePost()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    if (isset($_POST['ref_images'])) {
      $arrayimages = array();
      foreach ($_POST['ref_images'] as $ref_images) {
        array_push($arrayimages,$ref_images);
      }
      $_SESSION['arrayimage'] = $arrayimages;
      echo json_encode(array('error'=>'0', 'message'=>'Image(s) associée(s) !'));
    }
    elseif (isset($_FILES['path']['name'])) {
      $image = new Image($this->getDB());
      $testImageExistante = (new Image($this->getDB()))->all();

      $rootImage = "../../ressources/images/articles/";
      $cheminImage = $rootImage . $_FILES['path']['name'];

      $array = array ("path" => $cheminImage);

      $boolImageExistante = 0;
      foreach ($testImageExistante as $testImage)
      {
        if ($testImage->path == $cheminImage)
        {
          $boolImageExistante = 1;
          break;
        }
      }

      if ($boolImageExistante == 0) {
      $result = $image->insert($array);
        if ($result) {
          $images = (new Image($this->getDB()))->selectImagesDispos();
          echo json_encode(array('error'=>'0', 'message'=>'Ajout de l\'image réussi !', 'images' => $images));
        }
        else {
          echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de l\'ajout de l\'image !'));
        }
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Cette image est déjà dans la base de données !'));
      }
    }
    elseif (isset($_POST['nom']) && !isset($_POST['description'],$_POST['ref_marques'],$_POST['prix'],$_POST['ref_categories'])) {
      $marque = new Marque($this->getDB());
      $testMarqueExistante = (new Marque($this->getDB()))->all();

      $boolMarqueExistante = 0;
      foreach ($testMarqueExistante as $testMarque)
      {
        if ($testMarque->nom == $_POST['nom'])
        {
          $boolMarqueExistante = 1;
        }
      }

      if ($boolMarqueExistante == 0) {
      $result = $marque->insert($_POST);
        if ($result) {
          $marques = (new Marque($this->getDB()))->all();
          echo json_encode(array('error'=>'0', 'message'=>'Ajout de la marque réussi !', 'marques'=> $marques));
        }
        else {
          echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de l\'ajout de la marque !'));
        }
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Cette marque est déjà dans la base de données !'));
      }
    }
    elseif (isset($_POST['nom'],$_POST['description'],$_POST['ref_marques'],$_POST['prix'],$_POST['ref_categories'])) {
      $article = new Article($this->getDB());
      $testArticleExistant = (new Article($this->getDB()))->all();

      $boolArticleExistant = 0;
      foreach ($testArticleExistant as $testArticle)
      {
        if ($testArticle->nom == $_POST['nom'] && $testArticle->description == $_POST['description'] && $testArticle->prix == $_POST['prix'] && $testArticle->ref_categories == $_POST['ref_categories'] && $testArticle->ref_marques == $_POST['ref_marques'])
        {
          $boolArticleExistant = 1;
        }
      }

      if ($boolArticleExistant == 0) {
        $result = $article->insert($_POST);
        if ($result) {
          if (isset($_SESSION['arrayimage'])) {
            $dernierarticle = (new Article($this->getDB()))->getDernierIdArticle();
            $image_article = new Image_Article($this->getDB());
            foreach ($_SESSION['arrayimage'] as $key => $value) {
              $array = array('ref_images' => $value, 'ref_articles' => $dernierarticle[0]->id);
              $image_article->insert($array);
              $associee = array ("associee" => 1);
              $image = new Image($this->getDB());
              $image->update($value, $associee);
            }
            unset($_SESSION['arrayimage']);
          }
          $images = (new Image($this->getDB()))->selectImagesDispos();
          echo json_encode(array('error'=>'0', 'message'=>'Ajout de l\'article réussi !', 'images' => $images));
        }
        else {
          echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de l\'ajout de l\'article !'));
        }
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Cet article est déjà dans la base de données !'));
      }
    }
  }

  public function editImage(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $image = (new Image($this->getDB()))->findById($id);

    return $this->view('admin/editImage', compact('image'));
  }

  public function updateImage(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $image = new Image($this->getDB());
    $testImageExistante = (new Image($this->getDB()))->all();

    $rootImage = "../../ressources/images/articles/";
    $cheminImage = $rootImage . $_FILES['path']['name'];

    $array = array ("path" => $cheminImage);

    $boolImageExistante = 0;
    foreach ($testImageExistante as $testImage)
    {
      if ($testImage->path == $cheminImage)
      {
        $boolImageExistante = 1;
        break;
      }
    }

    if ($boolImageExistante == 0) {
    $result = $image->update($id, $array);
      if ($result) {
        echo json_encode(array('error'=>'0', 'message'=>'Modification de l\'image réussie !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de la modification de l\'image !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Cette image est déjà dans la base de données !'));
    }
  }

  public function deleteImage(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $image = new Image($this->getDB());
    $result = $image->delete($id);

    if ($result) {
      return header('Location: /admin?success=true');
    }
  }

  public function newImage()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    return $this->view('admin/newImage');
  }

  public function newImagePost()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $image = new Image($this->getDB());
    $testImageExistante = (new Image($this->getDB()))->all();

    $rootImage = "../../ressources/images/articles/";
    $cheminImage = $rootImage . $_FILES['path']['name'];

    $array = array ("path" => $cheminImage);

    $boolImageExistante = 0;
    foreach ($testImageExistante as $testImage)
    {
      if ($testImage->path == $cheminImage)
      {
        $boolImageExistante = 1;
        break;
      }
    }

    if ($boolImageExistante == 0) {
    $result = $image->insert($array);
      if ($result) {
        echo json_encode(array('error'=>'0', 'message'=>'Ajout de l\'image réussi !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de l\'ajout de l\'image !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Cette image est déjà dans la base de données !'));
    }
  }

  public function imageArticle(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $articles = (new Article($this->getDB()))->all();
    $image = (new Image($this->getDB()))->findById($id);

    return $this->view('admin/imageArticle', compact('articles','image'));
  }

  public function imageArticlePost()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $image = new Image($this->getDB());
    $imageArticle = new Image_Article($this->getDB());
    $testImageAssociee = (new Image_Article($this->getDB()))->all();

    $boolImageAssociee = 0;
    foreach ($testImageAssociee as $testAssociation)
    {
      if ($testAssociation->ref_images == $_POST['ref_images'])
      {
        $boolImageAssociee = 1;
        break;
      }
    }

    $array = array ("associee" => 1);

    if ($boolImageAssociee == 0) {
    $result = $imageArticle->insert($_POST);
    $image->update($_POST['ref_images'], $array);
      if ($result) {
        echo json_encode(array('error'=>'0', 'message'=>'Association image/article réussie !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de l\'association entre l\'image et l\article !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Cette image est déjà associée à un article dans la base de données !'));
    }
  }

  public function deleteImageArticle(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $image = new Image($this->getDB());
    $image_article = new Image_Article($this->getDB());
    $associee = (new Image_Article($this->getDB()))->findById($id);

    $array = array ("associee" => 0);
    $image->update($associee->ref_images, $array);

    $result = $image_article->delete($id);

    if ($result) {
      return header('Location: /admin?success=true');
    }
  }

  public function editMarque(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $marque = (new Marque($this->getDB()))->findById($id);

    return $this->view('admin/editMarque', compact('marque'));
  }

  public function updateMarque(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $marque = new Marque($this->getDB());
    $testMarqueExistante = (new Marque($this->getDB()))->all();

    $boolMarqueExistante = 0;
    foreach ($testMarqueExistante as $testMarque)
    {
      if ($testMarque->nom == $_POST['nom'])
      {
        $boolMarqueExistante = 1;
      }
    }

    if ($boolMarqueExistante == 0) {
      $result = $marque-> update($id, $_POST);
      if ($result) {
        echo json_encode(array('error'=>'0', 'message'=>'Modification de la marque réussie !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de la modification de la marque !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Cette marque est déjà dans la base de données !'));
    }
  }

  public function deleteMarque(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $marque = new Marque($this->getDB());
    $result = $marque->delete($id);

    if ($result) {
      return header('Location: /admin?success=true');
    }
  }

  public function newMarque()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    return $this->view('admin/newMarque');
  }

  public function newMarquePost()
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $marque = new Marque($this->getDB());
    $testMarqueExistante = (new Marque($this->getDB()))->all();

    $boolMarqueExistante = 0;
    foreach ($testMarqueExistante as $testMarque)
    {
      if ($testMarque->nom == $_POST['nom'])
      {
        $boolMarqueExistante = 1;
      }
    }

    if ($boolMarqueExistante == 0) {
    $result = $marque->insert($_POST);
      if ($result) {
        echo json_encode(array('error'=>'0', 'message'=>'Ajout de la marque réussi !'));
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de l\'ajout de la marque !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Cette marque est déjà dans la base de données !'));
    }
  }

  public function deleteCommande(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $infocommande = (new Commande($this->getDB()))->findById($id);
    $infoutilisateur = (new Utilisateur($this->getDB()))->findById($infocommande->ref_utilisateurs);

    $date = date_create($infocommande->dates);
    $dates = date_format($date,"d/m/Y");
    $to = $infoutilisateur->email;
    $subject = 'Un administrateur a annulé votre commande du '.$dates.'.';
    $msg = "<html>
              <body>
                <h3>Un administrateur a annulé votre commande qui avait le n° ".$infocommande->id."</h3>
              </body>
            </html>";
    $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
    $headers .= "MIME-version: 1.0\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
    mail($to, $subject, $msg, $headers);


    $commande = (new Commande($this->getDB()))->findById($id);
    $result = $commande->delete($id);

    if ($result) {
      return header('Location: /admin?success=true');
    }
  }

  public function detailsCommande(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $commandes = (new Commande($this->getDB()))->selectCommandesById($id);
    $commandes_articles = (new Commande_Article($this->getDB()))->selectCommandesArticlesByRefCommandes($id);
    $statuts = (new Statut($this->getDB()))->all();

    return $this->view('admin/detailsCommande', compact('commandes','commandes_articles','statuts'));
  }

  public function updateStatut(int $id)
  {
    if (!$this->isAdmin()) {
      return header('Location: /');
    }

    $commande = (new Commande($this->getDB()))->findById($id);

    if ($commande->ref_statuts != $_POST['ref_statuts']) {
      $result = $commande->update($id, $_POST);
      if ($result) {
          echo json_encode(array('error'=>'0', 'message'=>'Modification du statut de la commande effectuée !'));

          $infoutilisateur = (new Utilisateur($this->getDB()))->findById($commande->ref_utilisateurs);
          $commandestatut = (new Commande($this->getDB()))->findById($id);
          $infostatut = (new Statut($this->getDB()))->findById($commandestatut->ref_statuts);

          $date = date_create($commande->dates);
          $dates = date_format($date,"d/m/Y");
          $to = $infoutilisateur->email;
          $subject = "Votre commande n°".$commande->id." du ".$dates." a changé de statut !";
          $msg = "<html>
                    <body>
                      <h3>Son statut est désormais : ".$infostatut->nom."</h3>
                    </body>
                  </html>";
          $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
          $headers .= "MIME-version: 1.0\r\n";
          $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
          mail($to, $subject, $msg, $headers);
      }
      else {
          echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de la modification du statut de la commande !'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Cette commande a déjà ce même statut !'));
    }
  }

}
 ?>
