<?php
namespace App\Controllers;

use App\Models\Utilisateur;
use App\Models\Abonnement;

class UserController extends Controller
{
  public function connexion()
  {
    return $this->view('auth/connexion');
  }

  public function connexionPost()
  {
      if (!empty($_POST['email']) && !empty($_POST['mot_de_passe'])) {
        $utilisateurExiste = (new Utilisateur($this->getDB()))->utilisateurExiste($_POST['email']);
        if ($utilisateurExiste->existe == 1) {
          $utilisateur = (new Utilisateur($this->getDB()))->getByEmail($_POST['email']);
          $_SESSION['IdUtilisateurAuth'] = $utilisateur->id;
          $_SESSION['prenom'] = $utilisateur->prenom;
          $_SESSION['nom'] = $utilisateur->nom;
          $_SESSION['email'] = $utilisateur->email;
          $_SESSION['abonnement'] = $utilisateur->abonnement;

          if (password_verify($_POST['mot_de_passe'], $utilisateur->mot_de_passe) && $utilisateur->admin == 1)
          {
            $_SESSION['auth'] = $utilisateur->admin;
            echo json_encode(array('error'=>'0','message'=>'/admin?success=true'));
          }
          elseif (password_verify($_POST['mot_de_passe'], $utilisateur->mot_de_passe) && $utilisateur->admin != 1) {
            $_SESSION['auth'] = 0;
            echo json_encode(array('error'=>'0','message'=>'/'));
          }
          else {
            echo json_encode(array('error'=>'1', 'message'=>'Mauvaise combinaison adresse email/mot de passe. Veuillez réessayer.'));
          }
        }
        elseif ($utilisateurExiste->existe == 0) {
          echo json_encode(array('error'=>'1', 'message'=>'Aucun compte utilisateur n\'est associé à cette adresse email.'));
        }
      }
      else
      {
        echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de votre tentative de connexion. Veuillez réessayer.'));
      }
  }

  public function logout()
  {
    session_destroy();
    return header('Location: /');
  }

  public function inscription()
  {
    return $this->view('auth/inscription');
  }

  public function inscriptionPost()
  {
    $boolEmailExistant = 0;
    $testCompteExistant = (new Utilisateur($this->getDB()))->all();
    foreach ($testCompteExistant as $testEmail)
    {
      if ($testEmail->email == $_POST['email'])
      {
        $boolEmailExistant = 1;
      }
    }

    $utilisateur = new Utilisateur($this->getDB());

	   if (!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['email']) && !empty($_POST['mot_de_passe']))
     {
       $_POST['nom'] = htmlspecialchars(strip_tags(ucwords(mb_strtolower($_POST['nom']))));
       $_POST['prenom'] = htmlspecialchars(strip_tags(ucwords(mb_strtolower($_POST['prenom']))));
       $_POST['email'] = htmlspecialchars(strip_tags($_POST['email']));
       $_POST['mot_de_passe'] = password_hash($_POST['mot_de_passe'], PASSWORD_DEFAULT);

       if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) && $boolEmailExistant == 0)
       {
         $result = $utilisateur->insert($_POST);
         echo json_encode(array('error'=>'0', 'message'=>'Merci pour votre inscription ! Veuillez <a href="/connexion" class="alert-link">Cliquez ici</a> pour vous connecter'));
         $to = $_POST['email'];
         $subject = 'Inscription réussie !';
         $msg = "<html>
                   <body>
                     <h3>Merci pour votre inscription ".$_POST['prenom']." !</h3>
                     <p>Vous pouvez désormais vous connecter pour réserver des terrains et commander dans la boutique !</p>
                   </body>
                 </html>";
         $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
         $headers .= "MIME-version: 1.0\r\n";
         $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
         mail($to, $subject, $msg, $headers);
	      }
        else
        {
          echo json_encode(array('error'=>'1', 'message'=>'Cet email est déjà utilisé par un autre utilisateur ou n\'est pas valide.'));
	      }
    }
    else
    {
      echo json_encode(array('error'=>'1', 'message'=>'Un problème est survenu lors de votre tentative d\'inscription. Veuillez réessayer.'));
    }
  }

  public function motdepasseoublie()
  {
    return $this->view('auth/motdepasseoublie');
  }

  public function motdepasseoubliePost()
  {
    $utilisateur = (new Utilisateur($this->getDB()))->getByEmail($_POST['email']);
    //code de génération de mot_de_passe
    $caracteres = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $motdepasse = array();
    $longueurcaracteres = strlen($caracteres) - 1;
    for ($i = 0; $i < 10; $i++) {
      $n = rand(0, $longueurcaracteres);
      $motdepasse[] = $caracteres[$n];
    }
    $motdepasse = implode($motdepasse);
    //
    $utilisateur->updateMDP(password_hash($motdepasse, PASSWORD_DEFAULT),$_POST['email']);

    echo json_encode(array('error'=>'0', 'message'=>'Votre mot de passe a été réinitialiser. Nous vous avons envoyer un mail contenant votre nouveau mot de passe.'));

    $to = $_POST['email'];
    $subject = 'Votre mot de passe a été réinitialisé !';
    $msg = "<html>
              <body>
                <h3>Voici votre nouveau mot de passe : <b>".$motdepasse."</b> !</h3>
                <p>Nous vous conseillons de vous rendre sur le site pour le changer dans les plus bref délais.</p>
              </body>
            </html>";
    $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
    $headers .= "MIME-version: 1.0\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
    mail($to, $subject, $msg, $headers);
  }

  public function changermotdepasse()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    return $this->view('auth/changermotdepasse');
  }

  public function changermotdepassePost()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $utilisateur = (new Utilisateur($this->getDB()))->findById($_SESSION['IdUtilisateurAuth']);

    if (password_verify($_POST['mot_de_passe'], $utilisateur->mot_de_passe)) {
      if ($_POST['mot_de_passe2'] == $_POST['mot_de_passe3']) {
        if ($_POST['mot_de_passe'] != $_POST['mot_de_passe2']) {
          $utilisateur->updateMDP(password_hash($_POST['mot_de_passe2'], PASSWORD_DEFAULT), $_SESSION['email']);
          echo json_encode(array('error'=>'0', 'message'=>'Le changement de mot de passe a bien été effectué. Veuillez vous reconnecter.'));
          $to = $_SESSION['email'];
          $subject = 'Modification du mot de passe réussie !';
          $msg = "<html>
                    <body>
                      <h3>Vous devez désormais vous connecter avec votre nouveau mot de passe.</h3>
                    </body>
                  </html>";
          $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
          $headers .= "MIME-version: 1.0\r\n";
          $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
          mail($to, $subject, $msg, $headers);
        }
        else {
          echo json_encode(array('error'=>'1', 'message'=>'Attention, votre nouveau mot de passe correspond à votre mot de passe actuel.'));
        }
      }
      else {
        echo json_encode(array('error'=>'1', 'message'=>'Attention, vous n\'avez pas bien confirmer votre nouveau mot de passe.'));
      }
    }
    else {
      echo json_encode(array('error'=>'1', 'message'=>'Attention, votre mot de passe actuel n\'est pas correct.'));
    }
  }

  public function abonnement()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $prix_abonnement = (new Abonnement($this->getDB()))->all();

    return $this->view('auth/abonnement', compact('prix_abonnement'));
  }

  public function abonnementPost()
  {
    if (!$this->isAuth()) {
      return header('Location: /');
    }

    $utilisateur = (new Utilisateur($this->getDB()))->findById($_SESSION['IdUtilisateurAuth']);

    $utilisateur->update($_SESSION['IdUtilisateurAuth'], $_POST);

    if ($_POST['abonnement'] == 0) {
      $_SESSION['abonnement'] = 0;
      $to = $utilisateur->email;
      $subject = 'Aurevoir '.$_SESSION['prenom'].' !';
      $msg = "<html>
                <body>
                  <h3>Nous sommes désolés d'apprendre votre désabonnement ".$_SESSION['nom']." ".$_SESSION['prenom']." ...</h3>
                  <h3>Vous ne pouvez plus profiter des nombreux avantages de l'abonnement à notre club !</h3>
                </body>
              </html>";
      $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
      $headers .= "MIME-version: 1.0\r\n";
      $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
      mail($to, $subject, $msg, $headers);
    }
    elseif ($_POST['abonnement'] == 1) {
      $_SESSION['abonnement'] = 1;
      $to = $utilisateur->email;
      $subject = 'Affiliation réussie !';
      $msg = "<html>
                <body>
                  <h3>Merci pour votre abonnement à notre club ".$_SESSION['nom']." ".$_SESSION['prenom']." !</h3>
                  <h3>Vous profitez désormais de ses avantages !</h3>
                </body>
              </html>";
      $headers = "From: The Padel Place<thepadelplace@gmail.com>\r\n";
      $headers .= "MIME-version: 1.0\r\n";
      $headers .= 'Content-type: text/html; charset=utf-8'."\r\n";
      mail($to, $subject, $msg, $headers);
    }

    return header('Location: /abonnement');
  }

}
 ?>
